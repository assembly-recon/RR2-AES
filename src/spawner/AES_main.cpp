/*
 * @file AES_main.cpp
 * @brief AES execution for MVP4B
 *
 * (c) Maria I. Artigas (KU Leuven) 05.04.22
 *
 */
#include <thread>
#include <queue>
#include <mutex>
#include <functional>
#include <string>
#include <unistd.h>
#include <fstream>

#include "../world_model/ee_preemption.cpp"
#include "../communication/communication_infraflex.cpp"


#include "../spawner/task_spawner.cpp"
#include "../spawner/acr_connection.cpp"
#include "../spawner/res_connection.cpp"

// THESE QUEUES ARE USED FOR NOTIFICATION PURPOSES, NOT FOR BOOKEPPING
// int ORDER_TODO = 0; //ORDER GRAPHS TO BE DONE 
// int TASK_TODO = 1; // JOB (WORK DIRECTIVE) TO BE DONE
// int TASK_DONE = 2; // JOBS DONE 
// int ORDER_DONE = 3; // JOBS DONE 
// int PAUSE = 4; // JOBS DONE
// int TASK_DONE_acr = 5; // JOBS DONE NOTIFICATION TO THE ACR
// int TOOL_S = 6; // Tool state (to be changed to preemption petri net)

// int queue_number = 6;

// int MAX_ORDERS_QUEUE = 10;
// int MAX_TASK_QUEUE = 100;


// bool sim_acr = true;
// bool sim_res = false;
// bool preemption = true;


// As convention the tool state without attached tools is defined as "0"
//
// For future work: 
// - active_order should be a list, not a number
// - resources preemption is taking into account only one resource -> make it for several
// - when getting the next task it only takes one of the next tasks to be performed 
//  which means, that in case there are more than one active petrinet, the next task to be chosen 
//  will only correspond to the latest petri net task which arrived





std::string* unpause_order(std::queue<std::string>** queues, std::mutex** mutexes,petrinet_state_t **orders, int* order, std::string **tool_state )
{
    start_petrinet(orders[*order]);

    // if(preemption_management){

    //     // ADD PREEMTION
    //     std::string P1 = return_initial_preemption_task(*tool_state[*order]);
    //     std::string P2 = return_preemption_task(*tool_state[*order]);
        
    //     std::cout << "Preemption 1 = " << P1 <<std::endl;
    //     std::cout << "Preemption 2 = " << P2 <<std::endl;

    //     mutexes[TASK_TODO]->lock();
    //     queues[TASK_TODO]->emplace(P1);
    //     queues[TASK_TODO]->emplace(P2);
    //     mutexes[TASK_TODO]->unlock();
    // }

    return tool_state[*order];

}

void pause_order(petrinet_state_t **orders, int* order )
{
    stop_petrinet(orders[*order]);

    // KEEP STATUS OF RESOURCES ATTACHED TO THE ORDER, SO WHEN IT COMES BACK THE 0RDER -> IT CAN RESTART FROM THERE
    // PROBLEM:  MAKE SURE YOU PASS THIS BY VALUE, OTHERWISE IT WILL BREAK THE UPDATE
    // std::string done_task = *order_names[*order];

    // int new_order = *orders_in_process - 1;
    // *orders_in_process= new_order;
    // *active_order= new_order;
}



void update_done_tasks(std::queue<std::string>** queues, std::mutex** mutexes,petrinet_state_t **orders, int* orders_in_process,int* active_order,graph * world_model,std::string **tool_state )
{
   
    // std::cout << "before updating 2" << std::endl;
    // std::cout << "\n NEEDTOUPDATE 564864868454653455453" << std::endl;
    while(!(queues[TASK_DONE]->empty())){

        mutexes[TASK_DONE]->lock();
        std::string task = queues[TASK_DONE]->front();
        queues[TASK_DONE]->pop();
        mutexes[TASK_DONE]->unlock();


        // Tool status updated at resource level
        // std::string status = update_tool(world_model,preemption_tool,task);
        std::string status = get_tool_status(world_model,preemption_tool);
        *tool_state[*active_order] =status;

        std::cout << "\n ACTIVE ORDER TO BE UPDATED = " << *active_order <<std::endl;
        std::cout << "TOOL STATE = " << *tool_state[*active_order] <<std::endl;
        for(int index = 0;index<*orders_in_process;index++){

            std::cout << "ORDER :   " << index << std::endl;
            std::cout << "TOOL STATUS :   " << *tool_state[index] << std::endl;

        }

        // notification to the ACR
        mutexes[TASK_DONE_acr]->lock();
        queues[TASK_DONE_acr]->push(task);
        mutexes[TASK_DONE_acr]->unlock();

        update_petrinet(orders[*active_order],task,true);
        write_petrinet(orders[*active_order]->petri_net);
        // sleep(1.5);

    }
    
}

// FOR NOW THE LAST DONE PETRI NET IS NOT DELETED BUT TAKEN OUT OF THE ONES IN PROGRESS
// IN CASE A NEW ORDER COMES IN, IT WOULD TAKE THE PLACE OF THE LAST PETRINET STATE WHICH IS NOT IN USE
// THE POLICY IS TO RETAKE THE LAST TASK WHICH WAS BEING PERFORMED

//Gives back the state needed from resources
void update_done_order(std::queue<std::string>** queues, std::mutex** mutexes,petrinet_state_t **orders, std::string **order_names,int* active_order ,int* orders_in_process ,std::string** tool_state)
{
    stop_petrinet(orders[*active_order]);

    // PROBLEM:  MAKE SURE YOU PASS THIS BY VALUE, OTHERWISE IT WILL BREAK THE UPDATE
    std::string done_task = *order_names[*active_order];
    write_petrinet(orders[*active_order]->petri_net);

    std::cout << "deactive of order  " << *active_order << std::endl;

    mutexes[ORDER_DONE]->lock();
    queues[ORDER_DONE]->emplace(done_task);
    mutexes[ORDER_DONE]->unlock();

    int new_order = *orders_in_process - 1;
    *orders_in_process= new_order;
    *active_order= (new_order-1);
    std::cout << "number of orders c" << *orders_in_process << std::endl;
    std::cout << "active of orders " << *active_order << std::endl;
    std::cout << "done order " << done_task << std::endl;
    if(*orders_in_process > 0){unpause_order(queues,mutexes,orders,active_order,tool_state);};

}

void delete_order(petrinet_state_t **orders, int* order,int* orders_in_process,int* active_order )
{

    int new_order = *orders_in_process - 1;
    *orders_in_process= new_order;
    *active_order= (new_order-1);

    for(int i = *order; i< *orders_in_process ; i++)
    {
        orders[i] = orders[i+1];
    }

}


void loop_over_petri_nets(std::queue<std::string>** queues, std::mutex** mutexes,petrinet_state_t **orders,std::string **order_names, int* active_order, int* orders_in_process,std::string *LAST_TASK ,graph * world_model,std::string **tool_state ,bool* paused){

    update_done_tasks(queues,mutexes,orders,orders_in_process,active_order,world_model,tool_state);
    // print_flags(orders[*order_in_process]->conversion_map);

    std::string next_task = get_next_ready_task(orders[*active_order],"end");

        //     std::cout << "NEXT TASK IN THE QUEUE" << std::endl;
        // std::cout << next_task << std::endl;
    if(next_task == "DONE"&& (next_task != *LAST_TASK)){
        std::cout << "NEXT TASK IN THE QUEUE" << std::endl;
        std::cout << next_task << std::endl;
        // print_flags(orders[*active_order]->conversion_map);
        std::cout << "number of orders d" << *orders_in_process << std::endl;
         
        update_done_order(queues,mutexes,orders,order_names,active_order,orders_in_process,tool_state);
        *LAST_TASK = next_task;

    }
    // std::cout << next_task << std::endl;
    // std::cout << *LAST_TASK << std::endl;
    else if((next_task != "")&& (next_task != *LAST_TASK)&&!(*paused)){//&& (next_task != *LAST_TASK)){
        std::cout << "number of orders " << *orders_in_process << std::endl;
        std::cout << "NEXT TASK IN THE QUEUE" << std::endl;
        std::cout << next_task << std::endl;
        // print_flags(orders[*active_order]->conversion_map);
        update_done_tasks(queues,mutexes,orders,orders_in_process,active_order,world_model,tool_state);

        std::string work_dir = get_work_dir_id(next_task);

        // std::string preemption = check_preemtion(work_dir);

        // std::cout << "PREEMPTION NEEDED "<< preemption << std::endl;
        std::cout << "task to be dispatched "<< work_dir << std::endl;
        if(preemption_management)
        {

            const char * des_state = get_node_id(post_state(world_model,next_task.c_str())[0]);
            bool change_needed = false;
            bool state_from_task = false;
            if (des_state == NULL)
            {
                des_state = (*tool_state[*active_order]).c_str();

            } else {state_from_task = true;}

            const char * current_tool;
            current_tool = get_tool_status(world_model,preemption_tool);

            if(current_tool != des_state){change_needed = true;}

            std::cout << "STATE TO WHICH COME BACK 2 " << des_state << std::endl;
            if(change_needed)
            {
                // ADD PREEMTION
                const char ** preemption_t = preemption_tasks(world_model, current_tool, des_state);
                mutexes[TASK_TODO]->lock();
                bool task_done = false;
                for(int i = 0; i<sizeof(preemption_t);i++)
                {
                    if(preemption_t[i] != work_dir)
                    {
                        queues[TASK_TODO]->emplace(preemption_t[i]);
                    }else{task_done = true;}
                }
                if(!task_done){queues[TASK_TODO]->emplace(next_task);}
                mutexes[TASK_TODO]->unlock();
            } else if(state_from_task)
            {
                std::cout << "NO NEED TO CHANGE TOOL " << std::endl;
                *tool_state[*active_order] =current_tool;
                update_petrinet(orders[*active_order],next_task,true);

            } else
            {
                mutexes[TASK_TODO]->lock();
                queues[TASK_TODO]->emplace(next_task);
                mutexes[TASK_TODO]->unlock();
            }

        } else{

            mutexes[TASK_TODO]->lock();
            queues[TASK_TODO]->emplace(next_task);
            mutexes[TASK_TODO]->unlock();


        }
        *LAST_TASK = next_task;
    }   
}


bool pause_execution(std::queue<std::string>** queues, std::mutex** mutexes,petrinet_state_t **orders, std::string **order_names ,int* orders_in_process){

    std::cout << "NUMBER OF ORDERS TO BE PAUSED : "<< *orders_in_process << std::endl;
    for(int index = 0;index<*orders_in_process;index++){
        int* i = (int*)malloc(sizeof(int*));
        *i = index;
        pause_order( orders, i);
        std::cout << "EXECUTION PAUSED " << std::endl;
    }

    std::cout << "OUT OF LOOP " << std::endl;
    // When pause is received it should wait until the resources are done before saying the pause is feasible
    mutexes[PAUSE]->lock();
    queues[PAUSE]->push("paused_task_managent");
    mutexes[PAUSE]->unlock();
    bool paused = false;
    // std::cout << "after lock" << std::endl;
    while(!(queues[PAUSE]->empty())&& !paused){
        std::string pause = queues[PAUSE]->front();
        if(pause == "res_paused"){
            mutexes[PAUSE]->lock();
            queues[PAUSE]->pop();
            mutexes[PAUSE]->unlock();
            paused = true;
        }
    }
    std::cout << "THE RESOURCES HAVE NOTIFIED THAT THEY ARE PAUSED " << std::endl;


    return true;


}


//We are assuming that the last order in the orders array is the active one
void task_management(std::queue<std::string>** queues, std::mutex** mutexes, graph * world_model){

    std::string initial_status = get_tool_status(world_model,preemption_tool);

    bool not_done = true;
    bool pause = false;
    bool* paused = (bool*)malloc(sizeof(bool*));
    paused = &pause;
    int active_petrinet = 0;
    // Struc with number of petri nets, which one is active and the messages 
    // curl_global_init(CURL_GLOBAL_ALL);
    petrinet_state_t **orders = (petrinet_state_t**)malloc(MAX_ORDERS_QUEUE* sizeof(petrinet_state_t**));

    std::string **order_names = (std::string**)malloc(MAX_ORDERS_QUEUE* sizeof(std::string**));

    std::string **tool_state = (std::string**)malloc(MAX_ORDERS_QUEUE* sizeof(std::string*));

    // memset(*tool_state, 0, MAX_ORDERS_QUEUE*sizeof(*tool_state[0]));

    int* orders_in_process = (int*)malloc(sizeof(int*));
    int* active_order = (int*)malloc(sizeof(int*));
    int numb = 0;
    int act = 0;
    int tn = 0;
    task_n = &tn;
    orders_in_process = &numb;
    active_order = &act;
    std::string emp = "";
    std::string *LAST_TASK = &emp;
    std::cout << "INITIALIZATION FINE  " << std::endl;
    int order_counter = 0;
    while(not_done){

        //Listen to ACR task updates 
        //USUALLY THE ACTIVE ACTIVITY IS THE LAST TASK
        std::string order = "";
        mutexes[ORDER_TODO]->lock();
        // std::cout << "after lock" << std::endl;
        if(!(queues[ORDER_TODO]->empty())){
            std::cout << "RECEIVED ORDER" << std::endl;
            order = queues[ORDER_TODO]->front();
            queues[ORDER_TODO]->pop();
        }
        
        mutexes[ORDER_TODO]->unlock();

        mutexes[PAUSE]->lock();
        // std::cout << "after lock" << std::endl;
        if(!(queues[PAUSE]->empty())){
            std::string pause = queues[PAUSE]->front();
            if(pause == "in"){
                queues[PAUSE]->pop();
                mutexes[PAUSE]->unlock();
                pause_execution(queues,mutexes,orders,order_names,orders_in_process);
                std::cout << "  AFTER PAUSING" << std::endl;
                *paused = true;
                mutexes[PAUSE]->lock();
            }
        }
        
        mutexes[PAUSE]->unlock();


        // std::cout << "Orders in process" << *orders_in_process;
        if(*orders_in_process > 0){
            
            loop_over_petri_nets(queues,mutexes,orders, order_names,active_order,orders_in_process,LAST_TASK, world_model,tool_state,paused);
        }

        // std::cout << " next order : " << order << std::endl;
        if(order==""){}
        else{
            *paused = false;
            std::string ord = add_order_to_db(order);
            const char *nam = ord.c_str();
            std::cout << "  ORDER NAME " << std::endl;
            std::cout << *nam << std::endl;
            petrinet_state_t *petri_state = petri_net_mediator_start(order, "start","end",nam);
            std::cout << "ORDER NAME : " << order;
            std::cout << "orders in progress" << orders_in_process;
            orders[*orders_in_process]=petri_state;


            // Here the name of the petri net is assigned as its number
            order_names[*orders_in_process] = &ord;
            order_counter +=1;

            int new_order = *orders_in_process + 1;
            *orders_in_process= new_order;
            
            activate_petri_net(petri_state,"start");
            *active_order = *orders_in_process -1;

            //Might be sensitive to segmentation fault
            tool_state[*active_order] = &initial_status;
            write_petrinet(orders[*active_order]->petri_net);
            std::cout << "petri_net_activated" << std::endl;
            //Deactivating the other petri nets
            for(int index = 0;(index)<*active_order;index++){

                int* i = (int*)malloc(sizeof(int*));
                *i = index;
                // ask_for_preemption(queues,mutexes);
                pause_order(orders, i);
                if(!preemption_management){delete_order(orders, i, orders_in_process, active_order);}
            }
            
        }

        //Update ACR
        //If a petri net is at finish state -> delete task
        // If there are no tasks anymore keep listening to the ACR for a new task
        //Shut down from the ACR?
    }

}

// Before communication with the ACR, try managing the rush order by yourself

int main(int argc, char**argv){

    std::mutex** mutexes = (std::mutex**)malloc(queue_number * sizeof(std::mutex**));
    std::queue<std::string>** queues = (std::queue<std::string>**)malloc(queue_number * sizeof(std::queue<std::string>**));

    // for(int i= 0;i<queue_number;i++)
    // {
    //     std::mutex mutex;
    //     mutexes[i] = &mutex;
    //     std::queue<std::string> queue;
    //     queues[i] = &queue;
    // }
    std::queue<std::string> queue_1;
    std::queue<std::string> queue_2;
    std::queue<std::string> queue_3;
    std::queue<std::string> queue_4;
    std::queue<std::string> queue_5;
    std::queue<std::string> queue_6;
    // std::queue<std::string> queue_7;

    std::mutex mutex_1;
    std::mutex mutex_2;
    std::mutex mutex_3;
    std::mutex mutex_4;
    std::mutex mutex_5;
    std::mutex mutex_6;
    // std::mutex mutex_7;

    queues[0] = &queue_1 ;
    queues[1] = &queue_2 ;
    queues[2] = &queue_3 ;
    queues[3] = &queue_4 ;
    queues[4] = &queue_5 ;
    queues[5] = &queue_6 ;
    // queues[6] = &queue_7 ;

    mutexes[0] = &mutex_1 ;
    mutexes[1] = &mutex_2 ;
    mutexes[2] = &mutex_3 ;
    mutexes[3] = &mutex_4 ;
    mutexes[4] = &mutex_5 ;
    mutexes[5] = &mutex_6 ;
    // mutexes[6] = &mutex_7 ;

    graph * world_model = (graph*)malloc(sizeof(graph));
    world_model = init_world_model("../src/world_model/ee_status_model.jsonld");
    
    std::thread res(resource_management, queues,mutexes,world_model);
    sleep(1.1);
    std::thread task(task_management, queues,mutexes,world_model);
    sleep(0.8);
    std::thread acr(acr_connection, queues,mutexes);


    res.join();
    task.join();
    acr.join();

    return 0;
}