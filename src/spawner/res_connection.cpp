/*
 * @file res_connection.cpp
 * @brief Generic communication with resources at AES
 *
 * (c) Maria I. Artigas (KU Leuven) 05.05.22
 *
 */

const char preemption_tool[33] = "staubli_ee";

void resource_management(std::queue<std::string>** queues, std::mutex** mutexes, graph * world_model ){

    std::cout << "here" << std::endl;
    std::string b = queues[TASK_TODO]->front();
    std::cout << b << std::endl;

    bool up = true;
    if(!sim_res){ up = get_executor_info();std::cout << "EXECUTOR WAKING UP" << std::endl;}
    while(!up){up = get_executor_info(); std::cout << "EXECUTOR WAKING UP" << std::endl;}

    while(up){

        bool done = false;
        std::string task = "";
        // std::cout << "RESOURCES loop  " << std::endl;
        // std::cout << "before lock" << std::endl;
        mutexes[TASK_TODO]->lock();
        // std::cout << "after lock" << std::endl;
        if(!(queues[TASK_TODO]->empty())){
            task = queues[TASK_TODO]->front();
            queues[TASK_TODO]->pop();
            std::cout << "task to be handled by resources: " << task;
        }
        else{
            mutexes[TASK_TODO]->unlock();
            // The padding time is important to communicate back the last task before sending the pause event (TODO correction)
            sleep(4.5);
            mutexes[PAUSE]->lock();
            // std::cout << "after lock" << std::endl;
            if(!(queues[PAUSE]->empty())){
                std::string pause = queues[PAUSE]->front();
                if(pause == "paused_task_managent"){
                    // Padding time to make sure the last update is received before the notification of pause
                    

                    queues[PAUSE]->pop();
                    queues[PAUSE]->push("out");
                    queues[PAUSE]->push("res_paused");
                    std::cout << "RESOURCES PAUSED  " << std::endl;

                    //When pausing all the tasks ordered to the resource are cleaned
                    mutexes[TASK_TODO]->lock();
                    while(!(queues[TASK_TODO]->empty())){queues[TASK_TODO]->pop();}
                    mutexes[TASK_TODO]->unlock();


                }
            }
            mutexes[PAUSE]->unlock();
            continue;
        }

        mutexes[TASK_TODO]->unlock();
        if (task == ""){continue;}
        std::string work_dir = get_work_dir_id(task);
        std::cout << "wp to be handled by resources: " << work_dir << std::endl;
        std::cout << " \n \n \nabout to dispatch : "<< work_dir << std::endl;
        if(!sim_res){
            
            bool dispatched = dispatch_task(work_dir);
            if(!dispatched){
                //REPORT AND WAIT IF YOU CAN CONTINUE
                break;
            }
            bool triggered = trigger_task(work_dir);
            if(!triggered){
                //REPORT AND WAIT IF YOU CAN CONTINUE
                break;
            }
            while(!done){
                done = update_ping(work_dir);
            }}
        
        mutexes[TASK_DONE]->lock();
        task = wd2corrected(task);
        queues[TASK_DONE]->push(task);
        update_tool(world_model,preemption_tool,work_dir.c_str());
        std::cout << "\n UPDATING TASK = " << task <<std::endl;
        std::cout << "\n UPDATING TASK = " << work_dir <<std::endl;
        mutexes[TASK_DONE]->unlock();
        sleep(1.5);
    }

}