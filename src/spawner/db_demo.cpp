/*
 * @file db_demo.cpp
 * @brief Functionalities to share task ids between ACR and AES
 *
 * (c) Maria I. Artigas (KU Leuven) 05.04.22
 *
 */



std::string *task_ids = (std::string*)malloc(MAX_ORDERS_QUEUE* sizeof(std::string));
std::string *task_corrected = (std::string*)malloc(MAX_ORDERS_QUEUE* sizeof(std::string));
std::string *work_dir_ids = (std::string*)malloc(MAX_ORDERS_QUEUE* sizeof(std::string));
std::string *job_order_graph = (std::string*)malloc(MAX_ORDERS_QUEUE* sizeof(std::string));
int* task_n= (int*)malloc(sizeof(int));
std::mutex db_mutex;

// std::string domain_namespace = "http://kuleuven.com/"; 
std::string work_dir_id = "http://kuleuven.com/WorkDirective_Name";
std::string job_order_graph_id = "http://kuleuven.com/JobOrderGraph_ID"; 
std::string id_ref = "@id"; 


std::string substracting(std::string name){

    name.erase(remove(name.begin(), name.end(), '"'), name.end());    
    return name ; 

}

std::string substracting_namespace(std::string name){

    name = substracting(name);
    int e_strart = name.find(domain_namespace);
    name.erase(e_strart,domain_namespace.size());
    
    return name ; 

}
std::string formating(std::string name){

    name = substracting(name);

    // Keep only the last part of the id
    int e_strart = name.rfind("-");
    name.erase(0,e_strart+1);
    return ("ts"+name) ; 

}

std::string get_corrected_id(std::string id){
    
    db_mutex.lock();
    db_mutex.unlock();
    // std::string id2 = substracting_namespace(id);
    id = formating(id);
    return id;

}

std::string get_original_id(std::string corr_id){

    db_mutex.lock();
    db_mutex.unlock();
    for (int i = 0; i < *task_n; ++i)
    {
        if(corr_id == task_corrected[i]){
            return task_ids[i];
        }
    }
    return "";
}


std::string wd2original(std::string wd){

    db_mutex.lock();
    db_mutex.unlock();
    for (int i = 0; i < *task_n; ++i)
    {
        if(wd == work_dir_ids[i]){
            return task_ids[i];
        }
    }
    return wd;
}

std::string wd2corrected(std::string wd){

    db_mutex.lock();
    db_mutex.unlock();
    for (int i = 0; i < *task_n; ++i)
    {
        if(wd == work_dir_ids[i]){
            return task_corrected[i];
        }
    }
    return wd;
}


std::string get_work_dir_id(std::string corr_id){

    db_mutex.lock();
    db_mutex.unlock();
    for (int i = 0; i < *task_n; ++i)
    {
        // std::cout << *task_corrected[i] << std::endl;
        if(corr_id == task_corrected[i]){
            return work_dir_ids[i];
        }
    }
    return corr_id;
}
std::string get_job_order_graph_id(std::string corr_id){

    db_mutex.lock();
    db_mutex.unlock();
    for (int i = 0; i < *task_n; ++i)
    {
        if(corr_id == task_corrected[i]){
            return job_order_graph[i];
        }
    }
    return "";
}

std::string add_order_to_db(std::string order){

    padded_string graph_json{order};
    ondemand::parser parser;
    // *task_n = task_ids

    for (ondemand::object node : parser.iterate(graph_json)) {
        

        std::string_view name_v = simdjson::to_json_string(node[id_ref]);
        std::string task_id{name_v};
        // task_id = substracting(task_id);
        task_id = substracting_namespace(task_id);
        std::string corrected = get_corrected_id(task_id);

        std::string_view graph_v = simdjson::to_json_string(node[job_order_graph_id]);
        std::string g(graph_v);
        g = substracting(g);

        std::string_view wd_v = simdjson::to_json_string(node[work_dir_id]);
        std::string wd(wd_v);
        wd = substracting(wd);

        std::cout << "Adding task : " << std::endl;
        std::cout << task_id << std::endl;
        std::cout << corrected << std::endl;
        std::cout << g << std::endl;
        std::cout << wd << std::endl;


        db_mutex.lock();
        task_ids[*task_n] = task_id;
        task_corrected[*task_n] = corrected;
        job_order_graph[*task_n] = g;
        work_dir_ids[*task_n] = wd;
        db_mutex.unlock();



        *task_n = *task_n + 1;


  }
    std::cout << "Adding task LAST  : " << std::endl;
    std::cout << task_ids[*task_n-1] << std::endl;
    std::cout << task_corrected[*task_n-1] << std::endl;
    std::cout << work_dir_ids[*task_n-1] << std::endl;
    std::cout << job_order_graph[*task_n-1] << std::endl;


  std::cout << "JOB ORDER GRAPH ID " << std::endl;
  std::cout << job_order_graph[*task_n-1] << std::endl;
  return job_order_graph[*task_n-1];

}

