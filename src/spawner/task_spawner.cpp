/*
 * @file task_spawner.cpp
 * @brief Functionalities for automatic generation of Petri Net in the AES 
 *
 * (c) Maria I. Artigas (KU Leuven) 05.04.22
 *
 */
#include <iostream>
#include "../task_specification/jsonld_parser.cpp"
// #include "../coordination/petrinet_scheduler.c"

std::string start = "_start";
std::string end = "_wait";

std::string start_place_to_id(std::string name){

    // name.erase(remove(name.begin(), name.end(), '"'), name.end());
    int e_strart = name.find(start);
    name.erase(e_strart,domain_namespace.size());
    
    return name ; 

}

std::string id_to_end_place(std::string name){

    // name.erase(remove(name.begin(), name.end(), '"'), name.end());
    return (name+end) ; 

}


/**
 * Initializing the petri net.
 * When initializing the petri net state, the 
 * 
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
petrinet_state_t *petri_net_mediator_start(std::string task_spec_file_path,const char *start_flag_name,const char *end_flag_name,const char *petri_net_name){


    petrinet_state_t *state = init_petrinet_state(petri_net_name);
    std::cout << "ORDER NAME : " << petri_net_name;

    add_tasks_std(state,task_spec_file_path);
    add_precondtions_std(state,task_spec_file_path);
    add_resources_std(state,task_spec_file_path);
    // add_stable_places TODO
    append_initial_task_place_to_petrinet(state->petri_net,state->conversion_map,start_flag_name,false);
    append_ending_task_place_to_petrinet(state->petri_net,state->conversion_map,end_flag_name,false);
    write_petrinet(state->petri_net);

    return state;


}

petrinet_state_t *activate_petri_net(petrinet_state_t *state,const char *start_flag_name){

    state->active_petrinet = 1;
    int flags_n =  (int)(state->conversion_map->converting_sources.number_of_flags);
    for(int index=0; index<flags_n; index++){
        std::cout << "trying to activate petri net"<< std::endl;
        std::cout << state->conversion_map->converting_sources.names[index]<< std::endl;
        std::cout << start_flag_name<< std::endl;
        std::string s1{state->conversion_map->converting_sources.names[index]};
        std::string s2{start_flag_name};
        if( s1 == s2){
            std::cout << "\n next task ready: " << start_flag_name;
            *state->conversion_map->converting_sources.flags[index] = true;
            return state; 
        }
    }

      return state;


}

std::string get_next_ready_task(petrinet_state_t *state,std::string end){
      
    communicate_token_flags_flag_map(state->petri_net,state->conversion_map);
    int flags_n =  (int)(state->conversion_map->tracking_sinks.number_of_flags);

    for(int index=0; index<flags_n; index++){

        if(*state->conversion_map->tracking_sinks.flags[index] == 1){
            // std::cout << "\n next task ready: " << state->conversion_map->tracking_sinks.names[index];
            std::string next{state->conversion_map->tracking_sinks.names[index]} ;

            if(next == end){
                return"DONE";
            }

            next = start_place_to_id(next);
            return next;

        }
    }
    
    return "";


}

// This function stops the petri net in a stable step by taking away 
// the continue token in the stable transitions
bool stop_petrinet(petrinet_state_t *state){

    state->active_petrinet = 0;

}
bool start_petrinet(petrinet_state_t *state){

    state->active_petrinet = 1;

}

// TO BE REDEFINED AS IT ONLY WORKS WITH WAIT PLACES
bool update_petrinet(petrinet_state_t *state,std::string flag_name, bool flag){

    int flags_n =  (int)(state->conversion_map->converting_sources.number_of_flags);
    std::string place_name = id_to_end_place(flag_name);
    std::cout << "\n flag to be updated:  " << place_name;
 
    for(int index=0; index<flags_n; index++){

        std::string s1{state->conversion_map->converting_sources.names[index]};
        if(s1 == place_name){
            *state->conversion_map->converting_sources.flags[index] = flag;
            std::cout << "\n flag:  " << state->conversion_map->converting_sources.names[index];
            std::cout << "\n changed to :  " << *state->conversion_map->converting_sources.flags[index];

        }
    }

    communicate_token_flags_flag_map(state->petri_net,state->conversion_map);
    write_petrinet(state->petri_net);
    return true;


}


