/*
 * @file AES_preemption.cpp
 * @brief AES preemption for MVP4B
 *
 * (c) Maria I. Artigas (KU Leuven) 05.04.22
 *
 */
#include <thread>
#include <queue>
#include <mutex>
#include <functional>
#include <string>
#include <unistd.h>
#include <fstream>

#include "../communication/communication_ACR.c"


int ORDER_TODO = 0; //ORDER GRAPHS TO BE DONE 
int TASK_TODO = 1; // JOB (WORK DIRECTIVE) TO BE DONE
int TASK_DONE = 2; // JOBS DONE 
int ORDER_DONE = 3; // JOBS DONE 
int PAUSE = 4; // JOBS DONE
int TASK_DONE_acr = 5; // JOBS DONE NOTIFICATION TO THE ACR
// int TOOL_S = 6; // Tool state (to be changed to preemption petri net)

int queue_number = 6;

int MAX_ORDERS_QUEUE = 100;
int MAX_TASK_QUEUE = 800;

bool sim_acr = true;
bool sim_res = true;
bool preemption_management = false;

#include "../spawner/db_demo.cpp"

//Listens to the ACR and pushes to the task queue
void acr_connection(std::queue<std::string>** queues,std::mutex** mutexes){

    if(!sim_acr){
        std::string LAST_ORDER_TODO = "";
        // std::string LAST_ORDER_DONE = "";

        curl_global_init(CURL_GLOBAL_ALL);
        update_aes_state("paused");
        sleep(1.1);
        // mutexes[PAUSE]->lock();
        // queues[PAUSE]->push("");
        // mutexes[PAUSE]->unlock();
        bool take = true;
        int take2 = 0;
        while(true)
        {

            // First update last task to the ACR then denote if the system has been paused
            mutexes[TASK_DONE_acr]->lock();
            // std::cout << "after lock" << std::endl;
            if(!(queues[TASK_DONE_acr]->empty())){
                std::string done = queues[TASK_DONE_acr]->front();
                std::string id_done =  get_original_id(done);
                if(id_done==""){id_done = wd2original(done);}
                std::string here;
                // std::cin>> here;

                std::cout << "UPDATE TO BE SENT TO THE ACR JO DONE " << get_work_dir_id(done)<< std::endl;
                // std::cin>> here;
                update_performedJO(id_done.c_str());
                // std::cin>> here;
                queues[TASK_DONE_acr]->pop();
            }
            mutexes[TASK_DONE_acr]->unlock();

            mutexes[PAUSE]->lock();
            // std::cout << "after lock" << std::endl;
            if(!(queues[PAUSE]->empty())){
                std::string pause = queues[PAUSE]->front();
                if(pause == "out"){
                    update_aes_state("paused");
                    queues[PAUSE]->pop();
                }
            }
            
            mutexes[PAUSE]->unlock();

            char *msg;
            msg = "";
            if(take2<2){msg = get_JOG();}
            // msg = get_JOG();
            std::string msgs(msg);
            // std::cout << "msg from ACR " << msgs<< std::endl;
            // std::cout << " msg from ACR ! " << msgs<< std::endl;
            std::string pause("PAUSE");


            if(msgs.find(pause) != std::string::npos){
                std::cout << "pause msg from ACR " << msgs<< std::endl;
                mutexes[PAUSE]->lock();
                queues[PAUSE]->push("in");
                mutexes[PAUSE]->unlock();
                // It will denote the pause in the next iteration when the system is already paused
                take = true;

            }


            std::string id("@id");
            // std::cout << "msg from ACR " << msgs<< std::endl;
            if(msgs.find(id) != std::string::npos){
                if(take){
                    std::cout << "msg from ACR " << msgs<< std::endl;
                    update_aes_state("active");
                    mutexes[ORDER_TODO]->lock();
                    queues[ORDER_TODO]->push(msgs);
                    mutexes[ORDER_TODO]->unlock();
                    take = false;
                    take2 +=1;
                }
            }
            sleep(0.1);
        }
    }
    else{

        std::string path = "../src/task_specification/ACR_example2.jsonld";

        std::ifstream input_file(path);
        std::string order = std::string((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());

        mutexes[ORDER_TODO]->lock();
        queues[ORDER_TODO]->push(order);
        mutexes[ORDER_TODO]->unlock();

        sleep(6.0);

        mutexes[PAUSE]->lock();
        queues[PAUSE]->push("in");
        mutexes[PAUSE]->unlock();

        std::string path2 = "../src/task_specification/ACR_example3.jsonld";

        std::ifstream input_file2(path2);
        order = std::string((std::istreambuf_iterator<char>(input_file2)), std::istreambuf_iterator<char>());



        sleep(2.1);
        mutexes[ORDER_TODO]->lock();
        queues[ORDER_TODO]->push(order);
        mutexes[ORDER_TODO]->unlock();


        // MAKE A WHILE LOOP FOR PRINTING THE MESSAGES WHICH WOULD BE SENT TO THE ACR

    }



}