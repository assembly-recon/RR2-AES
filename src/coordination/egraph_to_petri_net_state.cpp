/*
 * @file egraph_to_petri_net_state.cpp
 * @brief Generator of petri net from declared graph functionality
 *
 * (c) Maria I. Artigas (KU Leuven) 15.03.22
 *
 */


#include <coordination-libs/petrinet/petrinet.h>
#include <iostream>
#include <coordination-libs/petrinet/petrinet_scheduler.h>

transition_behaviour_t default_behavior = {
        .condition = cond_Black1,
        .consumption_behaviour=consume_Black1,
        .production_behaviour=produce_Black1
};

flag_token_conversion_map_t *init_flags(flag_token_conversion_map_t *conversion_map,const int max_flags){

    for(int index=0; index<max_flags; index++){

        conversion_map->converting_sources.flags[index] = new bool(true);
        conversion_map->tracking_sinks.flags[index] = new bool(false);
    }  
    return conversion_map;

}


petrinet_state_t *init_petrinet_state(const char *petrinet_name)
{
    const int max_flags = 1000;
    const char** conv_n = (const char**)malloc(100*max_flags * sizeof(char**));
    bool** conv_f = (bool**)malloc(max_flags * sizeof(bool**));
    const char** track_n= (const char**)malloc(100*max_flags * sizeof(char**));
    bool** track_f= (bool**)malloc(max_flags * sizeof(bool**));

    flag_token_conversion_map_t *conversion_map= (flag_token_conversion_map_t*) malloc(100 * sizeof(flag_token_conversion_map_t));
    conversion_map->converting_sources.names = conv_n;
    conversion_map->converting_sources.flags = conv_f;
    conversion_map->converting_sources.number_of_flags = 0;

    conversion_map->tracking_sinks.names = track_n;
    conversion_map->tracking_sinks.flags = track_f;
    conversion_map->tracking_sinks.number_of_flags = 0;

    init_flags(conversion_map,max_flags);

    petrinet_t *net = (petrinet_t*) malloc(sizeof(petrinet_t));

    net = init_petrinet(petrinet_name);    

    petrinet_state_t* state_p = (petrinet_state_t*) malloc(sizeof(petrinet_state_t));
    state_p->petri_net = net;
    state_p->conversion_map = conversion_map;
    state_p->active_petrinet = 0;
    return state_p; 

}





/**
 * Mandatory to add an initial place, otherwise the start transition
 * has no conditions and will keep triggering in a loop.
 * 
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
place_t *add_initial_task_place(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *name, bool init_continue){

    char result[100];
    strcpy(result,name);

    int i = conversion_map->converting_sources.number_of_flags;
    // // Create places and transitions
    place_t *p_start = create_place(p,name);
    const char* p1 = (const char*) malloc(100 * sizeof(char));
    strcpy(const_cast<char *>(p1),result);
    conversion_map->converting_sources.names[i] = p1;
    *conversion_map->converting_sources.flags[i] = init_continue;
    conversion_map->converting_sources.number_of_flags+=1;

    return p_start;

}

/**
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
place_t *add_end_task_place(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *name, bool init_continue){

    char result[100];
    strcpy(result,name);

    int i = conversion_map->tracking_sinks.number_of_flags;
    // // Create places and transitions
    place_t *p_start = create_place(p,name);
    const char* p1 = (const char*) malloc(100 * sizeof(char));
    strcpy(const_cast<char *>(p1),result);
    conversion_map->tracking_sinks.names[i] = p1;
    *conversion_map->tracking_sinks.flags[i] = init_continue;
    conversion_map->tracking_sinks.number_of_flags= i +1;

    return p_start;

}


// TODO: Only returns one start of the petri net -> return several starts 
/**
 * Returns first transition
 * 
 * @param p the petri net
 */
char *get_transition_without_conds(petrinet_t *p){

    bool is_start = false;
    for (Agnode_t *n = agfstnode(p); n; n = agnxtnode(p, n)) {
        if (!strcmp(agget(n, "type"), "transition")) {
            is_start = true;
            for (Agedge_t *e = agfstin(p, n); e; e = agnxtin(p, e)) {
                is_start = false;
                break;
                } 
            if(is_start){
                return agnameof(n);
            }
        }
    }
    return NULL;
}

/**
 * Returns last transition
 * 
 * @param p the petri net
 */
char *get_transition_without_triggers(petrinet_t *p){

    bool is_end = false;
    for (Agnode_t *n = agfstnode(p); n; n = agnxtnode(p, n)) {
        if (!strcmp(agget(n, "type"), "transition")) {
            is_end = true;
            for (Agedge_t *e = agfstout(p, n); e; e = agnxtin(p, e)) {
                is_end = false;
                break;
                } 
            if(is_end){
                return agnameof(n);
            }
        }
    }
    return NULL;
}


/**
 * Finds the initial place of the petri net (start stransition without conditions) 
 * and adds a initial place beforehand avoiding loops
 * 
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
void append_initial_task_place_to_petrinet(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *name, bool start_flag){

    char *start_trans = get_transition_without_conds(p);
    place_t *init = add_initial_task_place(p,conversion_map,name,start_flag);
    transition_t *t_task1  = get_transition( p,start_trans);

    agedge(p, init, t_task1, NULL, TRUE);
    

}

/**
 * Finds the initial place of the petri net (start stransition without conditions) 
 * and adds a initial place beforehand avoiding loops
 * 
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
void append_ending_task_place_to_petrinet(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *name, bool end_flag){

    char *start_trans = get_transition_without_triggers(p);
    place_t *end = add_end_task_place(p,conversion_map,name,end_flag);
    transition_t *t_task1  = get_transition( p,start_trans);

    agedge(p, t_task1, end, NULL, TRUE);
    

}



/**
 * Adding task places to the petri net.
 * Convention taken for now:
 * 2 places per atomic task: start and wait
 * Transition after the places to be triggered when the task is finished
 * Transition before places to be triggered when the task can start
 * 
 * @see create_place()
 * 
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
void add_task_place(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *name, bool done){

    char result[100];

    strcpy(result,name);
    int i = conversion_map->tracking_sinks.number_of_flags;
    // // Create places and transitions
    place_t *p_start = create_place(p,strcat(result ,"_start"));
    const char* p1 = (const char*) malloc(100* sizeof(char));
    strcpy(const_cast<char *>(p1),result);
    *conversion_map->tracking_sinks.flags[i] = done;
    conversion_map->tracking_sinks.names[i] = p1;

    conversion_map->tracking_sinks.number_of_flags+=1;
    strcpy(result,name);
    place_t *p_wait =create_place(p, strcat(result,"_wait"));
    const char* p2 = (const char*) malloc(100 * sizeof(char));
    strcpy(const_cast<char *>(p2),result);
    i = conversion_map->converting_sources.number_of_flags;

    conversion_map->converting_sources.names[i] = p2;

    *conversion_map->converting_sources.flags[i] = done;
    conversion_map->converting_sources.number_of_flags+=1;

    strcpy(result,name);
    transition_t *t_start  = create_transition(p,strcat(result,"_start_trans"));
    strcpy(result,name);
    transition_t *t_end  = create_transition(p,strcat(result,"_end_trans"));
    // Add default behavior to the transitions
    add_behaviour(t_start, &default_behavior);
    add_behaviour(t_end, &default_behavior);

    // Add arrows to the petri net
    agedge(p, t_start, p_start, NULL, TRUE);
    agedge(p, p_start, t_end, NULL, TRUE);
    agedge(p, p_wait, t_end, NULL, TRUE);
}

/**
 * Adding precondition relation in petri net coordination.
 * Convention taken for now:
 * 
 * @see create_place()
 * 
 * @param p the petri net to be changed
 * @param name the name of task to be added 
 */
void add_precondition_connection(petrinet_t *p,const char * task1,const char * task2){

    char result1[100];
    char result2[100];
    strcpy(result1,task1);
    strcpy(result2,task2);

    // add_task_place(p,task1);
    // add_task_place(p,task2);

    // Get transitions to be connected 
    
    transition_t *t_task2 = get_transition( p,strcat(result2,"_start_trans"));
    write_petrinet(p);
    transition_t *t_task1  = get_transition( p,strcat(result1,"_end_trans"));
    strcpy(result1,task1);
    strcat(result1,"_precond_to_");
    // int i = conversion_map->tracking_sinks.number_of_flags;
    place_t *connection_place = create_place(p, strcat(result1,task2));

    
    // TODO MAKE FUNCTION TO GET THE CURRENT CARDINALITY OF EDGES CONNECTED TO TRANSITIONS
    // SO THEN YOU CAN ADD MORE

    // Add arrows to the petri net
    agedge(p, connection_place, t_task2, NULL, TRUE);
    agedge(p, t_task1, connection_place, NULL, TRUE);
}

/**
 * Adding feasible (not available) resources to the petri net tasks.
 * Convention taken for now:
 * 
 * 
 * @see add_task_place()
 * 
 * @param p the petri net to be changed
 * @param name the name of task
 * @param resources list of feasible resources which play the same 
 *      role in the task
 */
void add_res_2_task(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *task_name,const char *resource, bool available){

    char result1[100];
    strcpy(result1,task_name);
    transition_t *res_trans  = create_transition(p,strcat(result1,"_res_trans"));
    add_behaviour(res_trans, &default_behavior);

    strcpy(result1,task_name);
    strcat(result1,"_");
    place_t *p_res =create_place(p, strcat(result1,resource)); 
    int a = conversion_map->converting_sources.number_of_flags;
    const char* p1 = (const char*) malloc(100 * sizeof(char));
    strcpy(const_cast<char *>(p1),result1);
    conversion_map->converting_sources.names[a] = p1;
    *conversion_map->converting_sources.flags[a] = available;
    conversion_map->converting_sources.number_of_flags+=1;


    printf("Adding transition %d\n", result1);
    // Add arrows to the petri net
    agedge(p, p_res, res_trans, NULL, TRUE);

    strcpy(result1,task_name);
    printf("Getting transition %d\n", result1);
    // Create the place to be connected to the trigger of the task
    place_t *connection_place = create_place(p, strcat(result1,"_res_node"));
    int i = conversion_map->tracking_sinks.number_of_flags;
    const char* p2 = (const char*) malloc(100 * sizeof(char));
    strcpy(const_cast<char *>(p2),result1);
    conversion_map->tracking_sinks.names[i] = p2;
    *conversion_map->tracking_sinks.flags[i] = available;
    conversion_map->tracking_sinks.number_of_flags+=1;


    strcpy(result1,task_name);
    printf("Getting transition %d\n", result1);
    transition_t *t_task1 = get_transition( p,strcat(result1,"_start_trans"));
    
    agedge(p, res_trans, connection_place, NULL, TRUE);
    agedge(p, connection_place, t_task1, NULL, TRUE);

    // Create flags associated with the resource 
}

/**
 * Adding extra place to stop the task in a stable place 
 * This functionality is built to be used when a task leaves the 
 * worldin a stable state ?
 * 
 * @param p the petri net to be changed
 * @param name the name of the task which leads to a stable state
 * @param resources list of feasible resources which play the same 
 *      role in the task
 */
void add_stable_place(petrinet_t *p,flag_token_conversion_map_t *conversion_map,const char *task_name, bool cont){

    char result1[100];
    strcpy(result1,task_name);

    place_t *connection_place = create_place(p, strcat(result1,"_continue"));
    int i = conversion_map->converting_sources.number_of_flags;
    const char* p2 = (const char*) malloc(100 * sizeof(char));
    strcpy(const_cast<char *>(p2),result1);
    conversion_map->converting_sources.names[i] = p2;
    *conversion_map->converting_sources.flags[i] = cont;
    conversion_map->converting_sources.number_of_flags+=1;


    strcpy(result1,task_name);
    printf("Getting transition %d\n", result1);
    transition_t *t_task1 = get_transition( p,strcat(result1,"_end_trans"));
    
    agedge(p, connection_place, t_task1, NULL, TRUE);
}

/**
 * Printing flag functionality
 * 
 * @param conversion_map map containing flags
 */
void print_flags(flag_token_conversion_map_t *conversion_map){

    std::cout << "number of tracking sinks :  " << conversion_map->tracking_sinks.number_of_flags;
    int flags_n =  (int)(conversion_map->converting_sources.number_of_flags);
    for(int index=0; index<flags_n; index++){
        std::cout << "\n name of flag " << conversion_map->converting_sources.names[index];
        std::cout << "\n value of flag " << *conversion_map->converting_sources.flags[index];
    }

    int flags_f =  (int)(conversion_map->tracking_sinks.number_of_flags);
    for(int index=0; index<flags_f; index++){
        std::string s = conversion_map->tracking_sinks.names[index];
        std::cout << "\n name of flag " << s;
        std::cout << "\n value of flag " << *conversion_map->tracking_sinks.flags[index];
    }

}


/**
 * Example of petri net construction
 * 
 */
petrinet_t compressor_example(){

 
    petrinet_state_t *state = init_petrinet_state("example");
    const char *camera= "laser";
    const char *ur10 = "ur10";


    add_task_place(state->petri_net,state->conversion_map, "step_1",false);
    add_task_place(state->petri_net,state->conversion_map, "start",true);
    add_precondition_connection(state->petri_net, "start","step_1");
    add_task_place(state->petri_net,state->conversion_map, "step_2",false);
    add_res_2_task(state->petri_net,state->conversion_map, "step_2",camera,true);
    add_precondition_connection(state->petri_net, "step_1","step_2");
    add_stable_place(state->petri_net,state->conversion_map, "step_1",true);
    add_task_place(state->petri_net,state->conversion_map, "step_3",false);
    add_precondition_connection(state->petri_net, "step_2","step_3");

    append_initial_task_place_to_petrinet(state->petri_net,state->conversion_map,"START",true);


    write_petrinet(state->petri_net);
    print_marking(state->petri_net);


    // flag_token_conversion_map_t *conversion_map_copy= &conversion_map_val;
    // // conversion_map = 
    // // flag_token_conversion_map_t *conversion_map= &conversion_map_val;
    print_flags(state->conversion_map);
    communicate_token_flags_flag_map(state->petri_net,state->conversion_map);

    for(int index=0; index<state->conversion_map->converting_sources.number_of_flags; index++){
        std::string s("step_2_wait");
        std::string s1(state->conversion_map->converting_sources.names[index]);
        if ( s == s1){
            *state->conversion_map->converting_sources.flags[index] = true;
            std::cout << "\n adding flag in task 2 wait " << std::endl;
        }

    }  

    print_flags(state->conversion_map);
    communicate_token_flags_flag_map(state->petri_net,state->conversion_map);
    print_flags(state->conversion_map);
    write_petrinet(state->petri_net);
}