/*
 * @file jsonld_parse.cpp
 * @brief Extraction from jsonld graph into petri net
 *
 * (c) Maria I. Artigas (KU Leuven) 10.03.22
 *
 */


#include <iostream>
#include "simdjson/simdjson.h"
#include "../coordination/egraph_to_petri_net_state.cpp"
#include "../coordination/petrinet_scheduler.c"

using namespace simdjson;


std::string domain_namespace = "http://kuleuven.com/"; 
std::string id = "http://kuleuven.com/WorkDirective_Name";
std::string precond_rel = "http://kuleuven.com/needs_precond"; 
std::string res_rel = "http://kuleuven.com/needs_resource"; 

std::string substract_namespace(std::string name){

	name.erase(remove(name.begin(), name.end(), '"'), name.end());
	int e_strart = name.find(domain_namespace);
	name.erase(e_strart,domain_namespace.size());
	
	return name ; 

}
std::string format(std::string name){

	name.erase(remove(name.begin(), name.end(), '"'), name.end());

	// Keep only the last part of the id
	int e_strart = name.rfind("-");
	name.erase(0,e_strart+1);
	return ("ts"+name) ; 

}

void add_tasks(petrinet_state_t *state, std::string file_path){

	padded_string graph_json = padded_string::load(file_path);
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
    	
    	char task[100];
    	std::string_view name_v = simdjson::to_json_string(node[id]);

    	std::string name{name_v};
    	    // std::cout << name << std::endl;
    	name = format(name);
    	// std::cout << name << std::endl;
    	const char *n = name.c_str();
    	strcpy(task,n);
  		add_task_place(state->petri_net,state->conversion_map,task,false);
  		add_stable_place(state->petri_net,state->conversion_map,task,true);
  }
}

std::string get_id(std::string file_path, std::string_view name_v, std::string id1){

	padded_string graph_json = padded_string::load(file_path);
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
		char task[100];
    	std::string_view node_v = simdjson::to_json_string(node["@id"]);
    	// std::cout << name_v<< std::endl;
    	// std::cout << node_v<< std::endl;
    	// std::cout << "\n"<< std::endl;
    	if(node_v == name_v)
    	{
    		std::string_view req_id = simdjson::to_json_string(node[id1]);
    		std::string required_id{req_id};
    		return required_id;
    	}

	}
	return "";

}

void add_precondtions(petrinet_state_t *state, std::string file_path){

	padded_string graph_json = padded_string::load(file_path);
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
    	
    	char task[100];
    	std::string_view name_v = simdjson::to_json_string(node[id]);
    	
    	std::string name{name_v};
	    name = format(name);
	    const char *n = name.c_str();
	    strcpy(task,n);
	    ondemand::value obj;
	    auto error = node[precond_rel].get(obj);
	    std::cerr << "LEST'S TRANSLATE TASK "  << std::endl;
	    std::cerr << task  << std::endl;
	    if(error!= simdjson::SUCCESS) { std::cerr << "No preconditions found" << std::endl; }
	    else{
	    	for(ondemand::object precond : node[precond_rel]){

	    		char task2[100];
	    		std::string_view name_v = simdjson::to_json_string(precond["@id"]);
	    		// std::cout << name_v<< std::endl;
	    		std::string real_id = get_id(file_path, name_v, id);
		    	real_id = format(real_id);
		    	

		    	// std::cout << real_id<< std::endl;
		    	const char *n2 = real_id.c_str();
		    	strcpy(task2,n2);
		    	// THIS WILL HAVE TO BE CHANGED FOR THE RIGHT WORK OF THE PETRI NET
		    	// FIRST A CHECK WHETHER THAT TASK ALREADY EXISTS HAS TO BE DONE
		  		// add_task_place(net,task2,false);
		  		add_precondition_connection(state->petri_net,task2,task);

	    	}
	  	}

  }
}

//TO BE CORRECTED FOR THE NEW IDS
void add_resources(petrinet_state_t *state, std::string file_path){

	padded_string graph_json = padded_string::load(file_path);
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
    	
    	char task[100];
    	std::string_view name_v = simdjson::to_json_string(node[id]);
    	
    	std::string name{name_v};
	    name = format(name);
	    const char *n = name.c_str();
	    strcpy(task,n);
	    std::cout << "before error denotatio" << std::endl;
	    ondemand::value obj;
	    auto error = node[res_rel].get(obj);
	     std::cout << error << std::endl;
	    if(error!= simdjson::SUCCESS) { std::cerr << "No preconditions found" << std::endl; }
	    else{
	    	std::cout << "after error denotatio" << std::endl;
	    	for(ondemand::object resour : node[res_rel]){

	    		char res[100];
	    		std::cout << resour<< std::endl;
	    		std::string_view name_v = simdjson::to_json_string(resour["@id"]);
	    		std::string name2{name_v};
		    	name2 = format(name2);
		    	

		    	std::cout << name2<< std::endl;
		    	const char *n2 = name2.c_str();


		  		add_res_2_task(state->petri_net,state->conversion_map,task,n2,false);
	    	}

	  		std::cout << name << std::endl;
	  		std::cout << task << std::endl;
	  	}

  }
}





void add_tasks_std(petrinet_state_t *state, std::string graph){

	padded_string graph_json{graph};
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
    	
    	char task[100];
    	std::string_view name_v = simdjson::to_json_string(node["@id"]);

    	std::string name{name_v};
    	    // std::cout << name << std::endl;
    	name = substract_namespace(name);
    	name = format(name);
    	// std::cout << name << std::endl;
    	const char *n = name.c_str();
    	strcpy(task,n);
  		add_task_place(state->petri_net,state->conversion_map,task,false);
  		add_stable_place(state->petri_net,state->conversion_map,task,true);
  }
}

std::string get_id_std(std::string graph, std::string_view name_v, std::string id1){

	padded_string graph_json{graph};
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
		char task[100];
    	std::string_view node_v = simdjson::to_json_string(node["@id"]);
    	// std::cout << name_v<< std::endl;
    	// std::cout << node_v<< std::endl;
    	// std::cout << "\n"<< std::endl;
    	if(node_v == name_v)
    	{
    		std::string_view req_id = simdjson::to_json_string(node[id1]);
    		std::string required_id{req_id};
    		return required_id;
    	}

	}
	return "";

}

void add_precondtions_std(petrinet_state_t *state, std::string graph){

	padded_string graph_json{graph};
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
    	
    	char task[100];
    	std::string_view name_v = simdjson::to_json_string(node["@id"]);
    	
    	std::string name{name_v};
    	name = substract_namespace(name);
	    name = format(name);
	    const char *n = name.c_str();
	    strcpy(task,n);
	    ondemand::value obj;
	    auto error = node[precond_rel].get(obj);
	    std::cerr << "LEST'S TRANSLATE TASK "  << std::endl;
	    std::cerr << task  << std::endl;
	    if(error!= simdjson::SUCCESS) { std::cerr << "No preconditions found" << std::endl; }
	    else{
	    	for(ondemand::object precond : node[precond_rel]){

	    		char task2[100];
	    		std::string_view name_v = simdjson::to_json_string(precond["@id"]);
	    		// std::cout << name_v<< std::endl;
	    		// std::string real_id = get_id_std(graph, name_v, id);
		    	std::string real_id{name_v};
		    	real_id = substract_namespace(real_id);
		    	real_id = format(real_id);

		    	

		    	// std::cout << real_id<< std::endl;
		    	const char *n2 = real_id.c_str();
		    	strcpy(task2,n2);
		    	// THIS WILL HAVE TO BE CHANGED FOR THE RIGHT WORK OF THE PETRI NET
		    	// FIRST A CHECK WHETHER THAT TASK ALREADY EXISTS HAS TO BE DONE
		  		// add_task_place(net,task2,false);
		  		add_precondition_connection(state->petri_net,task2,task);

	    	}
	  	}

  }
}

//TO BE CORRECTED FOR THE NEW IDS
void add_resources_std(petrinet_state_t *state, std::string graph){

	padded_string graph_json{graph};
	ondemand::parser parser;
	for (ondemand::object node : parser.iterate(graph_json)) {
    	
    	char task[100];
    	std::string_view name_v = simdjson::to_json_string(node["@id"]);
    	
    	std::string name{name_v};
	    name = substract_namespace(name);
	    name = format(name);


	    const char *n = name.c_str();
	    strcpy(task,n);
	    std::cout << "before error denotatio" << std::endl;
	    ondemand::value obj;
	    auto error = node[res_rel].get(obj);
	     std::cout << error << std::endl;
	    if(error!= simdjson::SUCCESS) { std::cerr << "No preconditions found" << std::endl; }
	    else{
	    	std::cout << "after error denotatio" << std::endl;
	    	for(ondemand::object resour : node[res_rel]){

	    		char res[100];
	    		std::cout << resour<< std::endl;
	    		std::string_view name_v = simdjson::to_json_string(resour["@id"]);
	    		std::string name2{name_v};
		    	name2 = format(name2);
		    	name2 = substract_namespace(name2);
		    	

		    	std::cout << name2<< std::endl;
		    	const char *n2 = name2.c_str();


		  		add_res_2_task(state->petri_net,state->conversion_map,task,n2,false);
	    	}

	  		std::cout << name << std::endl;
	  		std::cout << task << std::endl;
	  	}

  }
}
