/*
 * @file preemption.cpp
 * @brief State of end effector + preemption functionalities in the AES
 *
 * (c) Maria I. Artigas (KU Leuven) 05.09.22
 *
 */
#include "traverse.c"
const char condition_c[33] = "status";

node* get_node_status(graph * g,const char tool[33] )
{
    Dt_t * result1 = NULL;
    node * n;
    n = get_node_from_db(g,tool);
    result1 = select_outgoing_edges( n, check_id_edge_condition, result1);
    EdgeCtn * edge_f = firstEdge(result1);
    node * tail = get_head(edge_f->edge_p);
    return tail;
}

const char* get_tool_status(graph * g,const char tool[33] )
{
    node * tail = get_node_status(g,tool);
    return get_sid_id(tail);
}

// Destroy previous status
bool destroy_previous_tool_status(graph * g,const char tool[33] )
{
    Dt_t * result1 = NULL;
    node * n;
    n = get_node_from_db(g,tool);
    result1 = select_outgoing_edges( n, check_id_edge_condition, result1);
    EdgeCtn * edge_f = firstEdge(result1);
    destroy_edge(g,edge_f->edge_p);
    return true;
}

// Connect to new status
bool add_new_tool_status(graph * g,const char tool[33],const char last_task[33] )
{
    
    node * tool_n;
    tool_n = get_node_from_db(g,tool);

    //add status rel between postset and tool
    sem_id_t sem_id_edge;
    strcpy( sem_id_edge.id, condition_c);
    strcpy( sem_id_edge.mid, "");
    strcpy( sem_id_edge.mmid, "0");
    node ** tail = post_state(g,last_task);
    if(tail !=NULL){create_edge(g,&sem_id_edge,tool_n,tail[0]);return true;}
    return false;
}

const char * update_tool(graph * g, const char tool[33],const char last_task[33])
{
    node ** ostatus = post_state(g,last_task);
    if(ostatus !=NULL){     
        destroy_previous_tool_status(g,tool);
        add_new_tool_status(g,tool,last_task);
    }
    const char * status = (char*)malloc(20*sizeof(char));
    graph_to_dot(g, "../output_updated.dot",true);
    status = get_tool_status(g,tool);
    return status;

}


// Function to get preemption tasks from initial state to the final state as 
// state to start preemtion
// For it, a simple breadth first search is used
node ** preemption_tasks_nodes(graph * g,const char initial_state[33],const char final_state[33])
{

    Dt_t * result1 = NULL;
    node * final_n = get_node_from_db(g,final_state);
    node * initial_n = get_node_from_db(g,initial_state);
    // Index of possible paths
    node ** task_path = breadth_first_search_states(g,initial_n,final_n);
        
    return task_path;
}

const char ** preemption_tasks(graph * g,const char initial_state[33],const char final_state[33])
{
    std::cout<< "getting preemption"<<std::endl;
    node ** preempt  = (node**)malloc(20*sizeof(node*));
    preempt = preemption_tasks_nodes(g,initial_state,final_state);
    std::cout<< "no problem getting preemption"<<std::endl;
    const char ** tasks  = (const char**)malloc(20*sizeof(char*));
    for(int i = 0; i<19;i++)
    {
        if(preempt[i]!=NULL){
            tasks[i] = (const char*)malloc(30*sizeof(char));std::cout<<tasks[i]<<"before ret"<<std::endl;
            tasks[i] = get_node_id(preempt[i]);std::cout<<tasks[i]<<"before ret"<<std::endl;
        }else{tasks[i] = NULL;}
    }
    std::cout<<"going out : "<<std::endl;
    return tasks;

}

// int main(int argc, char**argv){

//     // graph world_model = (graph)malloc(sizeof(graph));
//     graph * world_model = (graph*)malloc(sizeof(graph));
//     world_model = init_world_model("../src/world_model/ee_status_model.jsonld");
//     Dt_t * result;
//     // select_nodes( world_model, check_nodes, result);
//     const char tool[33] = "staubli_ee";
//     // std::cout<<get_tool_status(world_model,"staubli_ee")<<std::endl;
//     // destroy_previous_tool_status(world_model,"staubli_ee");
//     // add_new_tool_status(world_model,"staubli_ee","Pick_Parallel");
//     // get_tool_status(world_model,"staubli_ee");
//     graph_to_dot(world_model, "../output_before_updated.dot",true);
//     update_tool(world_model,"staubli_ee","Pick_Parallel");
//     graph_to_dot(world_model, "../output_updated.dot",true);
//     // node ** post;
//     // post = post_state(world_model,"Pick_Parallel");
//     // // std::cout<< "Needed task to get to parallel: " << get_node_id(needed_tasks(world_model,"Free")[0])<< std::endl;
//     // //     std::cout<< "Needed task to get to parallel: " << get_node_id(needed_tasks(world_model,"Free")[1])<< std::endl;
//     // // std::cout<< "Needed task to get to parallel: " << get_node_id(pre_state(world_model,"Pick_Parallel")[0])<< std::endl;
//     // // std::cout<< "Needed task to get to parallel: " << get_node_id(post[0]) << std::endl;
//     // std::cout<< "STARTING PREEMPTION QUERY " << std::endl;
//     const char ** preemption = (const char**)malloc(20*30*sizeof(char));
//     preemption = preemption_tasks(world_model,"ThreeFinger","Parallel");
//     // std::cout<< "Needed task to get to parallel: " << preemption_tasks(world_model,"ThreeFinger","Parallel")<< std::endl;
//     for(int k = 0; k <2; k++)
//     {
//         std::cout<<"content : "<<std::endl;
//         std::cout<<"content : "<<preemption[k]<<std::endl;
//         std::cout<<sizeof(preemption)<<std::endl;
//         if(preemption[k]!=NULL)
//         {
//             std::cout<< "Preemtpion task : " << preemption[k] << std::endl;
//         }
//     }

//     return 0;
// }

