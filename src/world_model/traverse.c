/*
 * @file preemption.cpp
 * @brief State of end effector + preemption functionalities in the AES
 *
 * (c) Maria I. Artigas (KU Leuven) 05.09.22
 *
 */
#include "json_ld_parser_world_model.cpp"

// The status of the robot is suppossed to have cardinality 1
bool check_id_edge_condition(const edge* edge1)
{
    const char * sid = get_edge_id(edge1);
    if(sid == condition){return true;}
    return false;
}
bool check_id_edge_postset(const edge* edge1)
{
    const char * id = get_edge_id(edge1);
    if(id == postset){return true;}
    return false;
}
bool check_id_edge_preset(const edge* edge1)
{
    const char * id = get_edge_id(edge1);
    if(id == preset){return true;}
    return false;
}
node * get_node_from_db(graph * g,const char name[33])
{
    char name_copy[33];
    strcpy(name_copy,name);
    strcat(name_copy,"::");
    return get_node(g,name_copy);

}
// This should give the postcond for the task passed by
node ** needed_tasks(graph * g, node * final_state ){

    Dt_t * result1 = NULL;
    //Get the postcondition
    result1 = select_incoming_edges( final_state, check_id_edge_postset, result1);
    if (result1 == NULL){return NULL;}
    EdgeCtn * edge_f = firstEdge(result1);
    node ** tasks= (node**)malloc(20*sizeof(node*));
    int i = 0;
    tasks[i] = get_tail(edge_f->edge_p);
    while(i<20){
        i++;
        edge_f = nextEdge(result1,edge_f);
        if(edge_f==NULL){
            tasks[i]= NULL;}
        else{tasks[i] = get_tail(edge_f->edge_p);}

    }
    return tasks;
}

node ** outgoing_nodes(graph * g, const char initial_node[33],const edge_selector select)
{
    Dt_t * result1 = NULL;
    node * n = get_node_from_db(g,initial_node);
    //Get the postcondition
    result1 = select_outgoing_edges( n, select, result1);
    if (result1 == NULL){return NULL;}
    EdgeCtn * edge_f; 
    int i = 0;
    edge_f= firstEdge(result1);
    node ** array= (node**)malloc(20*sizeof(node*));
    array[i] = get_head(edge_f->edge_p);
    while(i<20){
        i++;
        edge_f = nextEdge(result1,edge_f);
        if(edge_f==NULL){
            array[i]=NULL;}
        else{array[i] = get_head(edge_f->edge_p);}

    }
    return array;

}

//This should give the postcond for the task passed by
node ** post_state(graph * g, const char task[33]){

    node ** post = outgoing_nodes(g,task,check_id_edge_postset);
    return post;

}
node ** pre_state(graph * g, const char task[33]){

    node ** pre = outgoing_nodes(g,task,check_id_edge_preset);
    return pre;

}

bool has_loop(node ** path)
{
    for(int j = 0; j<20; j++)
    {
        if(path[j]!=NULL)
        {
            for(int i = j+1; i<20; i++)
            {
                if(path[i]!=NULL)
                {
                    if(path[j]== path[i]){return true;}
                }
            }
        }
    }
    return false;
}

// Revert backtracking
node ** invert(node ** input)
{

    int a = 0;
    for(int k = 19; k>=0; k--){if(input[k]!=NULL){a++;}}
    node ** output= (node**)malloc(a*sizeof(node*));;
    a = 0;
    for(int k = 19; k>=0; k--){if(input[k]!=NULL){output[a] = input[k];a++;}else{output[a]=NULL;}}
    for(int k = 19; k>=0; k--){if(input[k]!=NULL){}else{output[a]=NULL;a++;}}
    return output;
}

// Breadth first implementation with backtracking
node ** breadth_first_search_states(graph * g,node * initial_state, node * final_state)
{
    node * task_conc[20][20]= {NULL};
    node * paths[20][20]= {};
    int paths_count[20]= {};
    paths[0][0]= final_state;
    paths_count[0] = 1;
    bool done = false;
    while(!done)
    {
        //Check if the queue is empty
        if(paths_count[0]==0){break;}

        // Open paths
        node ** tasks = needed_tasks(g, paths[0][paths_count[0]-1]);
        node * initial[20];
        node * initial_tasks[20];
        for(int k = 0; k<20; k++)
            {
                initial[k] = paths[0][k];
                initial_tasks[k] = task_conc[0][k];
            }
        
        int initial_count = paths_count[0];
        int b = 0;
        int paths_n = 0;
        // Delete the first 
        for(int j = 0; j<20; j++)
        {
            for(int k = 0; k<20; k++)
            {
                paths[j][k] =paths[j+1][k];
                task_conc[j][k] =task_conc[j+1][k];
            }
            paths_count[j] = paths_count[j+1];
            if(paths[j][0]==NULL){paths_n= j; break;}
        }
        int added=0;
        while(tasks[b] != NULL)
        {
            node ** preconds = pre_state(g,get_node_id(tasks[b]));
            int a = 0;
            //Add new paths
            node ** new_path ;
            new_path= initial;
            node ** new_tasks = initial_tasks;
            new_path[initial_count] = preconds[0];
            new_tasks[initial_count-1] = tasks[b];
            if(!has_loop(new_path))
            {
                for(int k = 0; k<20; k++)
                {
                    paths[paths_n+added][k]= new_path[k]; 
                    task_conc[paths_n+added][k] =new_tasks[k];
                }
                paths_count[paths_n+added]= initial_count + 1 ;
                added++;
            }
            if(preconds[0]==initial_state){return invert(new_tasks);}
            b++;
        }
    }
    return NULL;
}
