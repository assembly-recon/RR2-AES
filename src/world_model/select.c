#include <world_model/select.h>

void setNode(NodeCtn* container, node* node) {
  container->node_p = node;
  container->node_id = get_node_id(node);
}

void setEdge(EdgeCtn* container, edge* edge) {
  container->edge_p = edge;
  container->edge_id = get_edge_id(edge);
}

/*
 * CDT "discipline" for dictionaries of NodeCtn objects
 */
Dtdisc_t NodeDiscipline = {
    sizeof(node*),  // offsetof(NodeCtn, node_id),
    -1,             // the ID is a char pointer
    -1,             // let CDT deal internally with the 'next' pointers
    0,
    0,
    0,
    0,
    0,
    0};

Dtdisc_t EdgeDiscipline = {
    sizeof(edge*),  // offsetof(EdgeCtn, edge_id),
    -1,             // the ID is a char pointer
    -1,             // let CDT deal internally with the 'next' pointers
    0,
    0,
    0,
    0,
    0,
    0};

Dt_t* select_nodes(graph* g, const node_selector select, Dt_t* result) {
  if (!g) return NULL;
  if (select == NULL) return NULL;
  if (result == NULL) {
    result = dtopen(&NodeDiscipline, Dtlist);
  }
  for (node* n = agfstnode(g); n; n = agnxtnode(g, n)) {
    if (select(n)) {
      NodeCtn* listItem = (NodeCtn*)malloc(sizeof(NodeCtn));
      setNode(listItem, n);
      dtappend(result, listItem);
    }
  }
  return result;
}

Dt_t* select_outgoing_edges(node* n, const edge_selector select,
                            Dt_t* result) {
  if (!n) return NULL;
  if (select == NULL) return NULL;
  if (result == NULL) {
    result = dtopen(&EdgeDiscipline, Dtlist);
    printf("SELECT_OUTGOING_EDGE :: opening edge list");
  }
  graph* g = agraphof(n);
  for (edge* e = agfstout(g, n); e; e = agnxtout(g, e)) {
    if (select(e)) {
      EdgeCtn* listItem = (EdgeCtn*)malloc(sizeof(EdgeCtn));
      setEdge(listItem, e);
      dtinsert(result, listItem);
    }
  }
  return result;
}

Dt_t* select_incoming_edges(node* n, const edge_selector select,
                            Dt_t* result) {
  if (!n) return NULL;
  if (select == NULL) return NULL;
  if (result == NULL) {
    result = dtopen(&EdgeDiscipline, Dtlist);
  }
  graph* g = agraphof(n);
  for (edge* e = agfstin(g, n); e; e = agnxtin(g, e)) {
    if (select(e)) {
      EdgeCtn* listItem = (EdgeCtn*)malloc(sizeof(EdgeCtn));
      setEdge(listItem, e);
      dtappend(result, listItem);
    }
  }
  return result;
}

NodeCtn* firstNode(Dt_t* list) {
  if (list) return ((NodeCtn*)dtfirst(list));
  return NULL;
}

NodeCtn* nextNode(Dt_t* list, NodeCtn* current) {
  void* n = dtnext(list, current);
  return ((NodeCtn*)n);
}

EdgeCtn* firstEdge(Dt_t* list) {
  if (list) return (EdgeCtn*)dtfirst(list);
  return NULL;
}

EdgeCtn* nextEdge(Dt_t* list, EdgeCtn* current) {
  void* n = dtnext(list, current);
  return ((EdgeCtn*)n);
}

bool hasOutEdges(const node* n) {
    graph* g = agraphof((node*)n);  // const_cast<node*>
    return( agfstout(g, (node*)n) != NULL);
}
