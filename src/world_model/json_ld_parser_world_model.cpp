/*
 * @file task_spawner.cpp
 * @brief task spawner for AES main for MVP4B
 *
 * (c) Maria I. Artigas (KU Leuven) 05.09.22
 *
 */
#include <iostream>
#include <cstring>
#include "graph4.c"
#include "select.c"
#include "simdjson/simdjson.h"
// #include "../task_specification/jsonld_parser.cpp"
// #include "../coordination/petrinet_scheduler.c"

// As convention the tool state without attached tools is defined as "0"
using namespace simdjson;
std::string id_ = "@id";
std::string preset = "preset"; 
std::string postset = "postset";
std::string condition = "status";

// graph *world_model = (graph*)malloc(sizeof(graph*));

node * node_jsonld2graph(graph *property_graph, ondemand::object node_n)
{
    char node_name[33];
    std::string_view name_v = simdjson::to_json_string(node_n[id_]);
    sem_id_t p1_sid;
    char mid[33] = "";
    char mmid[33] = "";

    ondemand::value obj;
    // auto error = node_n["type"].get(obj);
    // if(error!= simdjson::SUCCESS) { std::cerr << "No type found" << std::endl; }
    // else{
    //     std::string_view type_v = simdjson::to_json_string(node_n["type"]);
    //     std::string type{type_v};
    //     type.erase(remove(type.begin(), type.end(), '"'), type.end());
    //     strcpy(mid,type.c_str());
    // }

    std::string name{name_v};
    name.erase(remove(name.begin(), name.end(), '"'), name.end());
    const char *n = name.c_str();
    strcpy(node_name,n);
    strcpy( p1_sid.id, node_name);
    strcpy( p1_sid.mid, mid);
    strcpy( p1_sid.mmid, mmid);
    return create_node(property_graph, &p1_sid);

}



void add_nodes(graph *property_graph, std::string file_path){

    padded_string graph_json = padded_string::load(file_path);
    ondemand::parser parser;
    for (ondemand::object node_n : parser.iterate(graph_json)) {
        std::string id_of_node = "";
        node * p_main;
        
        for (auto field : node_n) {

            std::string_view keyv = field.unescaped_key();
            std::string key{keyv};
            if(key==id_)
            {

                p_main = node_jsonld2graph(property_graph,node_n);
                // print_sem_id(p_main);
                id_of_node = "1";

            } else if(key==preset || key==postset)
            {
                    for (auto rel_n : node_n[key])
                    {
                        for (auto cond_n : rel_n[condition])
                        {
                            char node_name[33];
                            std::string_view name_v = simdjson::to_json_string(cond_n[id_]);
                            char mid[33] = "";
                            char mmid[33] = "";
                            ondemand::value obj;
                            
                            std::string name{name_v};
                            name.erase(remove(name.begin(), name.end(), '"'), name.end());
                            key.erase(remove(key.begin(), key.end(), '"'), key.end());
                            condition.erase(remove(condition.begin(), condition.end(), '"'), condition.end());
                            const char *n = name.c_str();
                            strcpy(node_name,n);

                            sem_id_t p2_sid;
                            strcpy( p2_sid.id, node_name);
                            strcpy( p2_sid.mid, mid);
                            strcpy( p2_sid.mmid, mmid);
                            node * p2 = create_node(property_graph, &p2_sid);
                            // print_sem_id(p2);

                            if(id_of_node != "")
                            {
                                // EDGE creation
                                sem_id_t e1_sid;
                                strcpy( e1_sid.id, key.c_str());
                                strcpy( e1_sid.mid, condition.c_str());
                                strcpy( e1_sid.mmid, "0");
                                
                                create_edge(property_graph, &e1_sid, p_main, p2);

                            }else{std::cout<< "ERROR: No ID identified for the node with the connection "<< key <<" to "<< node_name << std::endl;}
                        }
                    }
                
                    
            }else if(key==condition)
            {
                for (auto rel_n : node_n[key])
                {
                    node * p2 = node_jsonld2graph(property_graph,rel_n);
                    key.erase(remove(key.begin(), key.end(), '"'), key.end());
                    condition.erase(remove(condition.begin(), condition.end(), '"'), condition.end());

                    // print_sem_id(p2);
                    if(id_of_node != "")
                        {
                            // EDGE creation
                            sem_id_t e1_sid;
                            strcpy( e1_sid.id, key.c_str());
                            strcpy( e1_sid.mid, "");
                            strcpy( e1_sid.mmid, "0");
                            create_edge(property_graph, &e1_sid, p_main, p2);

                        }else{std::cout<< "ERROR: No ID identified for the node with the connection "<< key << std::endl;}

                }

            }
        }
    }
    graph_to_dot(property_graph, "../output_ee_model.dot",true);

}
bool check_nodes(const node* n)
{
    return false;
}

std::string get_initial_status()
{

}

graph * init_world_model(std::string file_path)
{
    graph *world_model = create_property_graph();
    add_nodes(world_model,file_path);
    return world_model;

}