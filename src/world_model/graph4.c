
#include <cdt.h>
#include <cgraph.h>
#include <ctype.h>
#include <stdio.h>

#include <world_model/graph4.h>

static void set_semid_properties(graph* g) {
  agattr(g, AGNODE, "id", "");
  agattr(g, AGNODE, "mid", "");
  agattr(g, AGNODE, "mmid", "");
  agattr(g, AGEDGE, "id", "");
  agattr(g, AGEDGE, "mid", "");
  agattr(g, AGEDGE, "mmid", "");
}

graph* create_property_graph() {
  Agraph_t* g = agopen("Property Graph", Agdirected, NULL);
  set_semid_properties(g);
  return g;
}

int destroy_property_graph(graph* g) { return agclose(g); }

sem_id_t get_sem_id(const void* cobj) {
  void* obj = (void*)cobj;   // const_cast<void*>
  char* id = agget(obj, "id");
  char* mid = agget(obj, "mid");
  char* mmid = agget(obj, "mmid");
  sem_id_t sid;
  strncpy(sid.id, id, sizeof(sid.id));
  strncpy(sid.mid, mid, sizeof(sid.mid));
  strncpy(sid.mmid, mmid, sizeof(sid.mmid));
  return sid;
}

const char* get_sid_id  (const void* element) {
    if(element==NULL) return NULL;
    return agget((void*)element, "id");  // const_cast<void*>
}
const char* get_sid_mid (const void* element) {
    if(element==NULL) return NULL;
    return agget((void*)element, "mid");  // const_cast<void*>
}
const char* get_sid_mmid(const void* element) {
    if(element==NULL) return NULL;
    return agget((void*)element, "mmid");  // const_cast<void*>
}

const char* get_unique_id(const node* n) {
    if(!n) return NULL;
    return agnameof((node*)n); // const_cast<node*>
}


node* create_node(graph* g, sem_id_t* sem_id_node) {
    node_id nid;
    sem_id_to_node_id(sem_id_node, &nid);
    Agnode_t* node = agnode(g, nid.id, TRUE);

    if (NULL != node) {
        agset(node, "id"  , sem_id_node->id);
        agset(node, "mid" , sem_id_node->mid);
        agset(node, "mmid", sem_id_node->mmid);
    }
    return node;
}

void sem_id_to_node_id(const sem_id_t* sid, node_id* dest) {
    if ((!dest) || (!sid)) return;
    sprintf(dest->id, "%s:%s:%s", sid->id, sid->mid, sid->mmid);
}

node* get_node(graph* g, const char* id) {
  return agnode(g, (char*)id, FALSE);  // const_cast<char*>
}

node* get_node_by_sid(graph* g, const sem_id_t* sid) {
    node_id nid;
    sem_id_to_node_id(sid, &nid);
    return get_node(g, nid.id);
}

node* first_graph_node(graph* g) { return (node*)agfstnode(g); }

node* next_graph_node(graph* g, node* n) { return (node*)agnxtnode(g, n); }

void print_sem_id(const void* obj) {
  sem_id_t sid = get_sem_id(obj);
  printf("id: %s\tmid: %s\tmmid: %s\n", sid.id, sid.mid, sid.mmid);
}

// We "create the property" by using the bind-record function of cgraph; note
// that we deal internally with the cgraph requirement of having the record to
// begin with an Agrec_t, so that the user need not be aware of it.
// In addition, property_handle is added as little overhead
// This also means that the given size is only the size of the user data

static size_t prop_mem_off = sizeof(Agrec_t) + sizeof(property_handle);

// TODO Niko remove the undefined property.
static void* _create_property(void* thing, char* property_name,
                              size_t user_size) {
  if (thing) {
    void* mem =
        agbindrec(thing, property_name, prop_mem_off + user_size, FALSE);
    property_handle* ph = (property_handle*)((char*)mem + sizeof(Agrec_t));
    (*ph) = DEFINED;
    return ((char*)mem + prop_mem_off);
  }
  return NULL;
}

static void* _get_property(void* thing, const char* pname) {
  if (thing) {
    void* rec = aggetrec(thing, (char*)pname, 0);  // const_cast<char*>
    if (rec) {
      return ((char*)rec + prop_mem_off);
    }
  }
  return NULL;
}


// cgraph uses a special, internally managed record, to hold the
// references to the string attributes. We need to skip that record
// as it is not one of the user properties. The name of the record is
// stored in this variable

extern char* AgDataRecName;

// which is defined by cgraph but not declared in any public header.
// So it is not part of the public API and we can only hope it will
// stay the same...
// I noticed that a call to `agget` has the side effect of resetting
// the circular list of records so as to point to the one we want to
// skip. This has also the side effect that the records returned by
// us in these functions will reflect the order in which properties
// were added.
// Keep in mind that records in cgraph are arranged in a circular
// buffer, thus iteration will eventually lead again to this record

const void* get_first_property_record(const node* n) {
    if(n!=NULL) {
        // reset the circular buffer so that the first record will be the actual first
        agget((node*)n, "");  // const_cast<node*>

        const Agrec_t *d, *first;
        first = d = ((const Agobj_t*)n)->data;
        if(d != NULL) {
            //printf("AGDATA: %x   fist: %x\n", AGDATA(n), d);
            d = d->next; // skip the first
            if(d && d!=first) {
                return (const void*)d;
            }
        }
    }
    return NULL;
}

const void* get_next_property_record(const node* n, const void* current_property) {
    if(n==NULL || current_property==NULL) return NULL;
    const Agrec_t* curr = (const Agrec_t*)current_property;
    const Agrec_t* next = curr -> next;
    if( strcmp( next->name, AgDataRecName) == 0 ) {
        // skip the internal record when found. See comment above
        return (const void*)(next->next);
    }
    return (const void*)(next);
}

const char* property_name(const void* property_record) {
    if(property_record==NULL) return NULL;
    return ((const Agrec_t*)property_record)->name;
}
const void* property_data(const void* property_record) {
    if(property_record==NULL) return NULL;
    return (const void*)((char*)property_record + prop_mem_off);
}

bool has_properties(const node* n) {
    agget((node*)n, "");  // const_cast<node*>
    const Agrec_t *d = AGDATA(n);
    if(d!=NULL) {
        return (d!=d->next);
    }
    return false;
}


void* create_node_property(node* n, char* property_name, size_t user_size) {
  return _create_property(n, property_name, user_size);
}

void* get_node_property(node* n, const char* pname) {
  return _get_property(n, pname);
}

const void* get_property_const(const node* n, const char* name) {
  return _get_property((void*)n, (char*)name);  // const_cast<>
}

void* create_edge_property(edge* e, char* property_name, size_t user_size) {
  return _create_property(e, property_name, user_size);
}

void* get_edge_property(edge* e, char* pname) {
  return _get_property(e, pname);
}

const char* get_node_id(const node* n) {
  return agget((void*)n, "id");  // const_cast<void*>
}

const char* get_edge_id(const edge* e) {
  return agget((void*)e, "id");
}

const char* get_node_mid(const node* n) {
  return agget((void*)n, "mid");  // const_cast<void*>
}

const char* get_edge_mid(const edge* e) {
  return agget((void*)e, "mid");
}

const char* get_node_mmid(const node* n) {
  return agget((void*)n, "mmid");  // const_cast<void*>
}

const char* get_edge_mmid(const edge* e) {
  return agget((void*)e, "mmid");  // const_cast<void*>
}

//void create_undef_property(node* n, char* property_name) {
//  if (n) {
//    void* mem = agbindrec(n, property_name, prop_mem_off, FALSE);
//    property_handle* ph = (property_handle*)((char*)mem + sizeof(Agrec_t));
//    (*ph) = UNDEFINED;
//  }
//}
//
//int is_undef_prop(node* n, char* property_name) {
//  if (NULL != n) {
//    property_handle* ph =
//        (property_handle*)((char*)aggetrec(n, property_name, 0) +
//                           sizeof(Agrec_t));
//    if (UNDEFINED == (*ph)) return TRUE;
//  }
//  return FALSE;
//}

// void* replace_property(node* n, char* property_name, size_t dim) {
//   if(NULL!=n) {
//     if(TRUE == is_undef_prop(n,pname)) {
//       agdelrec(n,pname);
//       return create_property(n,property_name,dim);
//     }
//   }
//   return FALSE;
// }


int destroy_node_by_id(graph* g, char* id) {
  Agnode_t* n = agnode(g, id, FALSE);
  if (n) {
    return destroy_node(g, n);
  }
  return 0;
}


// EDGES
edge* create_edge(graph* g, sem_id_t* sem_id_edge, node* n1, node* n2) {
  Agedge_t* edge = agedge(g, n1, n2, sem_id_edge->id, TRUE);

  if (edge) {
    agset(edge, "id", sem_id_edge->id);
    agset(edge, "mid", sem_id_edge->mid);
    agset(edge, "mmid", sem_id_edge->mmid);
  }
  return edge;
}

edge* create_edge_by_id(graph* g, sem_id_t* sem_id_edge, char* id_n1,
                        char* id_n2) {
  Agnode_t *n1, *n2;
  n1 = agnode(g, id_n1, FALSE);
  if (n1) {
    n2 = agnode(g, id_n2, FALSE);
    if (n2)
      return create_edge(g, sem_id_edge, n1, n2);
    else
      return NULL;
  }
  return NULL;
}

edge* find_edge_by_id(graph* g, node* n1, node* n2, char* id) {
  return agedge(g, n1, n2, id, FALSE);
}


int destroy_node(graph* g, node* n) {
    return agdelnode(g, n);  // 0: success, -1: failure
}

int destroy_edge(graph* g, edge* e) {
    return agdeledge(g, e);
}

node* get_tail(edge* e) {
    if(e==NULL) return NULL;
    return agtail(e);
}

node* get_head(edge* e) {
    if(e==NULL) return NULL;
    return aghead(e);
}


static void serialize_properties(node* n, const char* nid, FILE* file)
{
    Agrec_t *d, *first;
    first = d = ((Agobj_t*)n)->data;
    short closeIt = FALSE;
    if(d != NULL) {
        d = d->next; // skip the first, which is some CGraph internal thing
        if(d && d!=first) {
            fprintf(file, "\"%s_props\" [shape=record, label=\"{", nid);
            closeIt = TRUE;
        }
    }
    short separator = FALSE;
    while(d && d!=first) {
        if(separator) {
            fprintf(file, " | ");
        }
        fprintf(file, "%s", d->name);
        d = d->next;
        separator = TRUE;
    }
    if(closeIt) {
        fprintf(file, "}\"];\n");
        fprintf(file, "\"%s\" -> \"%s_props\" [color=gray,label=\"properties\"];\n", nid, nid);
    }
}

// VISUALIZATION
void graph_to_dot(graph* g, char* filename, bool verbose) {
  FILE* fp = fopen(filename, "w");
  if (!fp) {
    fprintf(stderr, "Could not open file %s\n", filename);
    return;
  }
  // TODO verbose to be deprecated
  if (!verbose) {
    agwrite(g, fp);
  } else {
    Agnode_t* n;
    Agedge_t* e;
    fprintf(fp, "digraph \"Property Graph\" {\n");
    for (n = first_graph_node(g); n; n = next_graph_node(g, n)) {
      const char* node_id = get_node_id(n);
      const char* node_mid = get_node_mid(n);
      fprintf(fp, "\"%s\" [label=\"%s:%s\"];\n", node_id, node_mid, node_id);
      serialize_properties(n, node_id, fp);

      for (e = agfstout(g, n); e; e = agnxtout(g, e)) {
        const char* edge_id  = agget(e, "id");
        const char* edge_mid = agget(e, "mid");
        fprintf(fp, "\"%s\" -> \"%s\" [label=\"%s:%s\"];\n",
                node_id, agnameof(get_head(e)), edge_mid, edge_id);
      }
    }
    fprintf(fp, "}\n");
  }
  fclose(fp);
}

graph* dot_to_graph(const char* filename) {
  if (!filename) return NULL;
  FILE* fp = fopen(filename, "r");
  if (!fp) {
    fprintf(stderr, "Could not open file %s for reading.\n", filename);
    return NULL;
  }
  graph* g = agread(fp, NIL(Agdisc_t*));
  set_semid_properties(g);
  for (node* n = agfstnode(g); n; n = agnxtnode(g, n)) {
    agset(n, "id", agnameof(n));  // we let the id default to the node name
    for (edge* e = agfstout(g, n); e; e = agnxtout(g, e)) {
      agset(e, "id", "edge_def_id");
    }
  }
  fclose(fp);
  return g;
}
