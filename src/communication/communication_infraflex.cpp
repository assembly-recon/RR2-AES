/*
 * @file communication_infraflex.cpp
 * @brief Communication with infraflex cell coordinated with petri nets
 *
 * (c) Maria I. Artigas (KU Leuven) 01.04.22
 *
 */

#include <fstream>
#include <iostream>
#include "simdjson/simdjson.h"
#include "zmq/zmq-dealer.hpp"
using namespace simdjson;

std::string file_path_executor_info = "../src/communication/messages/get_info_event.json";

std::string file_path_dispatch = "../src/communication/messages/messages_dispatch.json";

// std::string file_path_trigger = "../src/communication/messages/messages_trigger.json";
std::string file_path_trigger = "../src/communication/messages/trigger.json";

std::string file_path_ping = "../src/communication/messages/new_ping.json";


deal::Dealer aes_o("tcp://10.102.210.101:5021", "aes_cell1");
deal::Dealer* aes = &aes_o;


bool get_executor_info(){

    // padded_string msg_v = padded_string::load(file_path_executor_info);
    // std::string_view v = simdjson::to_json_string(msg_v)
    // std::string msg{v};

    std::ifstream ifs(file_path_executor_info);
    const std::string content( (std::istreambuf_iterator<char>(ifs) ),
                       (std::istreambuf_iterator<char>()) );
    std::cout << "printing content" << std::endl;
    std::cout << content << std::endl;

    aes->send(content);
    std::string msg= "";
    aes->recv(msg,zmq::recv_flags::none);
    return true;

    // MAKE A TIMEOUT IN CASE THE EXECUTOR DOES NOT ANSWER
}

bool dispatch_task(std::string id){

    padded_string graph_json = padded_string::load(file_path_dispatch);
    std::cout << "printing dispatching" << std::endl;
    
    std::cout << id << std::endl;
    bool sent = false;
    bool accepted = false;
    ondemand::parser parser;
    for (ondemand::object node : parser.iterate(graph_json)) {
        
        char task[50];
        std::string_view wd_id = simdjson::to_json_string(node["work_directive_id"]);
        std::string wd{wd_id};
        wd.erase(remove(wd.begin(), wd.end(), '"'), wd.end());
        if(wd == id){
            std::string_view msg_v = simdjson::to_json_string(node["work_directive"]);
            const std::string msg{msg_v};
            std::cout << msg << std::endl;
            aes->send(msg);
            sent = true;
        }
    }
    // TODO: update if it does
    if(sent){
        std::string msg = "";
        aes->recv(msg,zmq::recv_flags::none);
        std::cout << "RECEIVE EVENT: " << std::endl;
        std::cout << msg << std::endl;
        accepted = true;
    }

    return accepted;
}

// Previous communication with INFRAFlex cell
// bool trigger_task(std::string id){

//     padded_string graph_json = padded_string::load(file_path_trigger);

//     bool sent = false;
//     bool accepted = false;
//     ondemand::parser parser;
//     for (ondemand::object node : parser.iterate(graph_json)) {
        
//         char task[50];
//         std::string_view wd_id = simdjson::to_json_string(node["work_directive_id"]);
//         std::string wd{wd_id};
//         wd.erase(remove(wd.begin(), wd.end(), '"'), wd.end());
//         if(wd == id){
//             std::string_view msg_v = simdjson::to_json_string(node["work_directive"]);
//             const std::string msg{msg_v};
//             std::cout << msg << std::endl;
//             aes->send(msg);
//             sent = true;
//         }
//     }
//     // TODO: update if it does
//     if(sent){
//         std::string msg = "";
//         aes->recv(msg,zmq::recv_flags::none);
//         std::cout << "RECEIVE EVENT: " << std::endl;
//         std::cout << msg << std::endl;
//         accepted = true;
//     }


//     return accepted;
// }

bool trigger_task(std::string id){

    bool done = false;
    std::ifstream ifs(file_path_trigger);
    const std::string content( (std::istreambuf_iterator<char>(ifs) ),
                       (std::istreambuf_iterator<char>()    ) );
    std::string msg = "";
    aes->send(content);
    aes->recv(msg,zmq::recv_flags::none);
    std::cout << "RECEIVE EVENT: " << std::endl;
    std::cout << msg << std::endl;
    // if task reading = 100% return true
    done = true;
    return done;
}

bool check_process(std::string msg){

    padded_string json{msg};
    ondemand::parser parser;
    auto doc = parser.iterate(json);
    auto event =doc.at_pointer("/event_type_");
    std::string_view evv = simdjson::to_json_string(event);
    std::string ev{evv};
    std::cout << "TYPE OF EVENT : "<< ev << std::endl;
     // doc.find_field("event_type_");
    std::string tr("\"task_update_event\"");
    if(ev == tr){
        double progress = doc.find_field("progress");
        if(progress == 100){
            return true;
            // }
        }
    }
    return false;
}

bool update_ping(std::string id){

    bool done = false;
    std::ifstream ifs(file_path_ping);
    const std::string content( (std::istreambuf_iterator<char>(ifs) ),
                       (std::istreambuf_iterator<char>()    ) );
    std::string msg = "";
    while(!done){
        aes->recv(msg,zmq::recv_flags::none); 
        std::cout << "RECEIVE EVENT: " << std::endl;
        std::cout << msg << std::endl;
        aes->send(content);
        aes->send(content);
        aes->recv(msg,zmq::recv_flags::none);
        std::cout << "RECEIVE EVENT: " << std::endl;
        std::cout << msg << std::endl;

        done = check_process(msg);

    }
    // if task reading = 100% return true
    done = true;
    return done;

    // MAKE A TIMEOUT IN CASE THE EXECUTOR DOES NOT ANSWER
}
