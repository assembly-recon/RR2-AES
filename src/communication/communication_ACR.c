/*
 * (c) Maria Artigas (KU Leuven) 15.04.22
 *
 */
#include <libcurl/aes_acr_comms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const char *addr = "10.102.210.101:8000";
#include "aes_acr_communication.c"

std::string task_specification_path = "../src/task_specification/complete_specification.jsonld";
std::string id_mvp = "http://kuleuven.com/WorkDirective_Name";

// std::string get_task_id( std::string task, std::string key){

//     padded_string graph_json = padded_string::load(task_specification_path);
//     ondemand::parser parser;
//     for (ondemand::object node : parser.iterate(graph_json)) {
        
//         std::string_view req_id = simdjson::to_json_string(node[key]);
//         std::string required_id{req_id};


//         std::cout << required_id<< std::endl;
//         std::cout << task<< std::endl;
//         if(required_id == task)
//         {
//             std::string_view node_v = simdjson::to_json_string(node["@id"]);
//             std::string req_id{req_id};
//             return req_id;
//         }

//     }
//     return "";

// }

std::string get_task_id( std::string task, std::string key){

    if(task == "Pick_ThreeFinger"){return "bf3b1809-568c-4e41-91bd-63d50e469e43";}
    if(task == "Insert_Rotor2_1"){return "ec8dc169-df08-4ecb-815b-1d28b13e54b1";}
    if(task == "Insert_Rotor1_1"){return "c71fc602-d4f7-4512-95ed-2b9340896724";}
    if(task == "Insert_Rotor2_2"){return "c0eaf841-78fc-4de0-ae73-4d6c04fbfdc4";}
    if(task == "Insert_Rotor1_2"){return "7d45483c-3315-4ed3-88d4-a75189f69d39";}
    if(task == "Drop_ThreeFinger"){return "70c24c00-cc4d-4ed9-abc6-bacb3507c0ad";}
    if(task == "Pick_Parallell"){return "45a4125b-b942-4707-963c-49773c5df413";}
    if(task == "Insert_Bearinghousing_1"){return "afee785e-ef40-407b-9b1f-5c600a0a56b1";}
    if(task == "Insert_Bearinghousing_2"){return "4325a072-9df8-4ef2-83d4-62d72992733a";}
    if(task == "Drop_Parallell"){return "f21b8eba-a55d-4d37-8c66-cf9463b12f74";}
}




bool post(const char *connection){

  // printf("%s\n",connection);
  CURL* handle;
  buffer_t send_buffer;
  send_buffer.data = (char*)malloc(10000 * sizeof(char*));
  send_buffer.size = 0;
  buffer_t receive_buffer;
  receive_buffer.data = (char*)malloc(10000 * sizeof(char*));
  receive_buffer.size = 0;
  if (!setup_handle_address(&handle, addr,connection,
                    (void*)&receive_buffer,
                    (void*)&send_buffer)) {
    printf("Could not create the connection");
  }

  send_data(handle, "");
  // printf("%s\n", receive_buffer.data);

  curl_easy_cleanup(handle);

  free(receive_buffer.data);
  free(send_buffer.data);
}

char *pull(const char *connection){

  // printf("%s\n",connection);
  CURL* handle;
  buffer_t send_buffer;
  send_buffer.data = (char*)malloc(10000 * sizeof(char*));
  send_buffer.size = 0;
  buffer_t receive_buffer;
  receive_buffer.data = (char*)malloc(10000 * sizeof(char*));
  receive_buffer.size = 0;
  if (!setup_handle_address(&handle, addr,connection,
                    (void*)&receive_buffer,
                    (void*)&send_buffer)) {
    printf("Could not create the connection");
  }
  
  get_data(handle, "");
  // printf("%s\n", receive_buffer.data);
  char *request = (char*)malloc(receive_buffer.size * sizeof(char*));
  memcpy(request, receive_buffer.data, receive_buffer.size);

  curl_easy_cleanup(handle);

  // free(receive_buffer.data);
  // free(send_buffer.data);
  return request;
}



// get_executor_info( "/api/PauseExecution")// NOT TO BE USED ACCORDING TO DOCUMENTATION???

// get_executor_info( "/api/JOPerformed/")

// THIS HAS TO BE PUT IN A LOOP
void update_performedJO(std::string JO){
    // std::string id_mvp = "http://kuleuven.com/WorkDirective_Name";
    // std::string id = get_task_id(JO,id_mvp);

    char result[10000];
    strcpy(result,"/api/JOPerformed/?joborder_id=");
    // printf("%s\n",result);
    strcat(result,JO.c_str()); 
    printf("\n%s\n",result);
    post(result);
}

// THIS HAS TO BE PUT IN A LOOP
char *get_JOG(){

  char result[10000];
  // printf("%s getting jog\n",result);
  strcpy(result,"/api/GetJOG/");
  // printf("%s\n",result);
  char *resp;
  resp =  pull(result);
  // std::cout << "msg from ACR " << resp<< std::endl;
  return resp;
}


void update_aes_state(char *status){

  char result[10000];
  strcpy(result,"/api/AESState/?assembly_state_message=");
  strcat(result,status); 
  printf("%s\n",result);
  post(result);
}






