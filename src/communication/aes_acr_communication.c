/*
 * (c) Nikolaos Tsiogkas (KU Leuven) 01.04.22
 *
 */

#include <libcurl/aes_acr_comms.h>
#include <stdlib.h>
#include <string.h>

char *form_https_address_from_ip_port(const char *ip_addr, const char *port) {
  // 8+16+5+1 http://192.192.192.192:65535\0
  size_t address_max_len = 29;
  char *address = (char *)malloc(address_max_len * sizeof(char));
  memset(address, '\0', address_max_len);
  strcpy(address, "http://");
  strcat(address, ip_addr);
  strcat(address, ":");
  strcat(address, port);
  return address;
}

char *form_enpoint_url_from_address(const char *address, const char *endpoint) {
  size_t url_size = strlen(address) + strlen(endpoint);
  char *url = (char *)malloc((url_size + 1) * sizeof(char));
  memset(url, '\0', url_size + 1);
  strcat(url, address);
  strcat(url, endpoint);
  return url;
}

bool setup_handle(CURL **handle, const char *ip_addr, const char *port,
                  const char *endpoint, void *recv, void *send) {
  
  char *address = form_https_address_from_ip_port(ip_addr, port);

  return setup_handle_address(handle,address,endpoint,recv,send);
}

bool setup_handle_address(CURL **handle, const char *addr,
                  const char *endpoint, void *recv, void *send) {
  *handle = curl_easy_init();
  if (!(*handle)) {
    printf("Failed to create CURL handle\n");
    return false;
  }

  char *url = form_enpoint_url_from_address(addr, endpoint);
  // curl_easy_setopt(*handle, CURLOPT_VERBOSE, 1L);
  if (curl_easy_setopt(*handle, CURLOPT_URL, url)) {
    printf("Failed to set the URL\n");
    return false;
  }
  // Free address and URL after use! libcurl makes internal copies
  // free(addr);
  free(url);

  if (curl_easy_setopt(*handle, CURLOPT_WRITEFUNCTION, write_cb)) {
    printf("Failed to set the write callback\n");
    return false;
  }

  if (curl_easy_setopt(*handle, CURLOPT_WRITEDATA, recv)) {
    printf("Failed to set the write data structure\n");
    return false;
  }

  if (curl_easy_setopt(*handle, CURLOPT_READFUNCTION, read_cb)) {
    printf("Failed to set the read callback\n");
    return false;
  }

  if (curl_easy_setopt(*handle, CURLOPT_READDATA, send)) {
    printf("Failed to set the read data structure\n");
    return false;
  }
  return true;
}

size_t write_cb(void *data, size_t size, size_t nmemb, void *userp) {
  size_t realsize = size * nmemb;
  buffer_t *mem = (buffer_t *)userp;

  char *ptr = (char*)realloc(mem->data, mem->size + realsize + 1);
  if (ptr == NULL) return 0; /* out of memory! */

  mem->data = ptr;
  memcpy(&(mem->data[mem->size]), data, realsize);
  mem->size += realsize;
  mem->data[mem->size] = 0;

  return realsize;
}

size_t read_cb(char *dest, size_t size, size_t nmemb, void *userp) {
  buffer_t *wt = (buffer_t *)userp;
  size_t buffer_size = size * nmemb;

  if (wt->size) {
    /* copy as much as possible from the source to the destination */
    size_t copy_this_much = wt->size;
    if (copy_this_much > buffer_size) copy_this_much = buffer_size;
    memcpy(dest, wt->data, copy_this_much);

    wt->data += copy_this_much;
    wt->size -= copy_this_much;
    return copy_this_much; /* we copied this many bytes */
  }

  return 0; /* no more data left to deliver */
}

void set_request(CURL *handle, const char *request) {
  size_t request_len = strlen(request);
  const char *data = "data to send";
  if (request_len > 0) {
    curl_easy_setopt(handle, CURLOPT_POSTFIELDSIZE, 12L);
    curl_easy_setopt(handle, CURLOPT_POSTFIELDS, data);
  }
  // printf("%s\n", request);
}

// Send data function (Makes a POST call)
void send_data(CURL *handle, char *request) {
  curl_easy_setopt(handle, CURLOPT_POST, 1L);
  set_request(handle, request);
  curl_easy_perform(handle);
}

// Read data function (Makes a GET call)
void get_data(CURL *handle, char *request) {
  curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);
  set_request(handle, request);
  curl_easy_perform(handle);
}
