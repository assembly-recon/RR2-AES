#ifndef ZMQ_REQUEST_HPP_INCLUDED
#define ZMQ_REQUEST_HPP_INCLUDED

#include <iostream>
#include <zmq.hpp>

namespace request
{
    class Request
    {
    private:
        zmq::context_t context_;
        zmq::socket_t socket_;
        bool s_send(const std::string &);
        std::string s_recv(zmq::recv_flags flags);

    public:
        Request(const char *socket_addr, const std::string identity);
        bool send(const std::string &message);
        bool recv(std::string &message);
        bool recv(std::string &message, zmq::recv_flags flags);
        bool set_identity(char *identity);
        ~Request();
    };

} // namespace request

#endif // !ZMQ_REQUEST_HPP_INCLUDED