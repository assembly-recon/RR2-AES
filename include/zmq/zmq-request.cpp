#include "zmq-request.hpp"

request::Request::Request(const char *socket_addr, const std::string identity) : socket_(context_, zmq::socket_type::req)
{
    socket_.set(zmq::sockopt::routing_id, identity);
    socket_.connect(socket_addr);
}

bool request::Request::s_send(const std::string &string)
{

    zmq::message_t message(string.size());
    memcpy(message.data(), string.data(), string.size());

    socket_.send(message, zmq::send_flags::none);
    return (true);
}

std::string request::Request::s_recv(zmq::recv_flags flags)
{
    // TODO change return type to std::optional
    zmq::message_t message;
    std::cout << "receiving ..." << std::endl;
    zmq::recv_result_t res = socket_.recv(message, flags);
    std::string message_string;
    if (res.has_value())
    {
        message_string = std::string(static_cast<char *>(message.data()), message.size());
    }
    else
    {
        message_string = "";
    }

    // std::cout << "Message: " <<  message << std::endl;

    return message_string;
}

bool request::Request::send(const std::string &message)
{
    s_send(message);
    return true;
}

bool request::Request::recv(std::string &message)
{
    message = s_recv(zmq::recv_flags::none);
    return true;
}
bool request::Request::recv(std::string &message, zmq::recv_flags flags)
{

    message = s_recv(flags);
    if (message != "")
        return true;
    else
        return false;
}

request::Request::~Request()
{
    std::cout << "Destroying Request object" << std::endl;
}
