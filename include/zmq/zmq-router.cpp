#include "zmq-router.hpp"

router::Router::Router(const char *socket_addr) : socket_(context_, zmq::socket_type::router)
{

    //socket_.set(zmq::sockopt::routing_id, identity);
    //maksocket_.setsockopt(ZMQ_ROUTER_MANDATORY, 1);
    //std::cout << "socket: " << socket_addr << std::endl;
    socket_.bind(socket_addr);
}

bool router::Router::s_sendmore(const std::string &string)
{
    zmq::message_t message(string.c_str(), string.length());
    //memcpy(message.data(), string.data(), string.size());
    std::cout << "Sending.." << std::endl;
    socket_.send(std::move(message), zmq::send_flags::sndmore);
    return (true);
}

bool router::Router::s_send(const std::string &string, int flags = 0)
{

    zmq::message_t message(string.size());
    memcpy(message.data(), string.data(), string.size());

    auto res = socket_.send(message, zmq::send_flags::none);
    std::cout << (res.has_value()) << std::endl;
    return (true);
}

std::string router::Router::s_recv()
{
    zmq::message_t message;
    socket_.recv(message);
    std::string message_string = std::string(static_cast<char *>(message.data()), message.size());
    return message_string;
}

bool router::Router::send(const std::string &recipient, const std::string &message)
{
    s_sendmore(recipient);
    s_sendmore("");
    s_send(message, 0);
    return true;
}

bool router::Router::recv(std::string &sender, std::string &message)
{
    sender = s_recv();
    s_recv();
    message = s_recv();
    return true;
}
router::Router::~Router()
{
    std::cout << "Destroying Router object" << std::endl;
    socket_.close();
}
