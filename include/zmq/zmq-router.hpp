#ifndef ZMQ_ROUTER_HPP_INCLUDED 
#define ZMQ_ROUTER_HPP_INCLUDED

#include <iostream>
#include <string>

#include <zmq.hpp>

namespace router
{
    class Router
    {
    private:
        bool s_sendmore(const std::string &);
        bool s_send(const std::string &, int);
        std::string s_recv();

        zmq::context_t context_;
        zmq::socket_t socket_;

    public:
        Router(const char *socket_addr);
        bool send(const std::string &recipient, const std::string &message);
        bool recv(std::string &sender, std::string &message);
        ~Router();
    };
} // namespace rout
#endif // !ZMQ_ROUTER_HPP_INCLUDED