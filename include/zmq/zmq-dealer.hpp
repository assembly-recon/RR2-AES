#ifndef ZMQ_DEALER_HEADER_GUARD
#define ZMQ_DEALER_HEADER_GUARD

#include <zmq.hpp>

namespace deal
{
    class Dealer
    {
    private:
        bool s_sendmore(const std::string &);
        bool s_send(const std::string &, int);
        std::string s_recv(zmq::recv_flags flags);

        zmq::context_t context_;
        zmq::socket_t socket_;

    public:
        Dealer(const char *socket_addr, const char *identity);
        bool send(const std::string &message);
        bool recv(std::string &message, zmq::recv_flags flags);
        ~Dealer();
    };

}
#endif // !ZMQ_DEALER_HEADER_GUARD
