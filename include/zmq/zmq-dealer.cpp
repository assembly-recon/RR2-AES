#include "zmq-dealer.hpp"
#include <iostream>

deal::Dealer::Dealer(const char *socket_addr, const char *identity) : socket_(context_, zmq::socket_type::dealer)
{

    socket_.set(zmq::sockopt::routing_id, identity);
    socket_.connect(socket_addr);
}

bool deal::Dealer::s_sendmore(const std::string &string)
{
    zmq::message_t message(string.c_str(), string.length());
    //memcpy(message.data(), string.data(), string.size());
    //std::cout << "Sending.." << std::endl;
    socket_.send(std::move(message), zmq::send_flags::sndmore);
    return (true);
}

bool deal::Dealer::s_send(const std::string &string, int flags = 0)
{

    zmq::message_t message(string.size());
    memcpy(message.data(), string.data(), string.size());
    auto res = socket_.send(message, zmq::send_flags::none);
    //std::cout << (res.has_value()) << std::endl;
    return (true);
}

std::string deal::Dealer::s_recv(zmq::recv_flags flags = zmq::recv_flags::none)
{
    zmq::message_t message;
    zmq::recv_result_t res = socket_.recv(message, flags);
    std::string message_string;
    if (res.has_value())
    {
        message_string = std::string(static_cast<char *>(message.data()), message.size());
    }
    else
    {
        message_string = "";
    }
    return message_string;
}

bool deal::Dealer::send(const std::string &message)
{
    s_sendmore("");
    s_send(message, 0);
    return true;
}

bool deal::Dealer::recv(std::string &message, zmq::recv_flags flags)
{
    s_recv(flags);
    message = s_recv(flags);
    return true;
}
deal::Dealer::~Dealer()
{
    socket_.close();
}
