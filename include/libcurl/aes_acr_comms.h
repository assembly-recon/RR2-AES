#ifndef AES_ACR_COMMS_H
#define AES_ACR_COMMS_H

#include <curl/curl.h>
#include <stdbool.h>

typedef struct buffer_s {
  char *data;
  size_t size;
} buffer_t;

// Setup function
bool setup_handle(CURL **handle, const char *ip_addr, const char *port,
                  const char *endpoint, void *recv, void *send);

bool setup_handle_address(CURL **handle, const char *addr,
                  const char *endpoint, void *recv, void *send);

// Reading Function (CURL reads using this)
size_t read_cb(char *dest, size_t size, size_t nmemb, void *userp);

// Writing Function (function that the port writes data to us)
size_t write_cb(void *data, size_t size, size_t nmemb, void *userp);

// Send data function (Makes a POST call)
void send_data(CURL *handle, char *request);

// Read data function (Makes a GET call)
void get_data(CURL *handle, char *request);

#endif  // AES_ACR_COMMS_H