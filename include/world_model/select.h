#ifndef ROOT_INCLUDE_RT_GRAPHKUL_SELECT_H_
#define ROOT_INCLUDE_RT_GRAPHKUL_SELECT_H_

#include <cdt.h>
#include <stdbool.h>
#include "graph4.h"

/**
 * \struct NodeCtn
 * \brief The container of a graph node.
 *
 * A container used to store a graph node and its' ID in a CDT list. The ID is
 * required from CDT to perform comparisons among with the other elements in
 * the listi.
 *
 * \var NodeCtn::node A pointer to a node in the graph.
 * \var NodeCtn::node_id A pointer to the ID of the node.
 */
typedef struct {
  node* node_p;
  const char* node_id;
}NodeCtn;

/**
 * \struct EdgeCtn
 * \brief The container of a graph edge.
 *
 * A container used to store a graph edge and its' ID in a CDT list. The ID is
 * required from CDT to perform comparisons among with the other elements in
 * the list.
 *
 * \var EdgeCtn::edge A pointer to an edge in the graph.
 * \var EdgeCtn::edge_id A pointer to the ID of the edge.
 */
typedef struct {
  edge* edge_p;
  const char* edge_id;
}EdgeCtn;

/**
 * \brief Add a node to a container.
 *
 * \param container A pointer to the container.
 * \param node A pointer to the node to be added.
 */
void setNode(NodeCtn* container, node* node);

/**
 * \brief Add an edge to a container.
 *
 * \param container A pointer to the container.
 * \param node A pointer to the edge to be added.
 */
void setEdge(EdgeCtn* container, edge* edge);

/**
 * \typedef A function pointer to a node selector function.
 */
typedef bool (*node_selector)(const node*);

/**
 * \typedef A function pointer to an edge selector function.
 */
typedef bool (*edge_selector)(const edge*);

/**
 * \brief Select a subset of nodes matching the selector criterie.
 *
 * Return a list of nodes of the graph matching the selection criteria. The
 * criteria is encoded in the node_selector argument, thus this function does
 * not really know anything about node properties.
 *
 * \param g The graph to select the nodes from.
 * \param select A function pointer to the function used to filter out nodes.
 * \param result A pointer to an already existing result list.
 * \return A pointer to a result list
 */
Dt_t* select_nodes(graph* g, const node_selector select, Dt_t* result);

/**
 * \brief Select a subset of edges matching the selector criterie.
 *
 * Return a list of edges of the node matching the selection criteria. The
 * criteria is encoded in the edge_selector argument, thus this function does
 * not really know anything about edge properties.
 *
 * \param g The node to select the edges from.
 * \param select A function pointer to the function used to filter out edges.
 * \param result A pointer to an already existing result list.
 * \return A pointer to a result list
 */
Dt_t* select_outgoing_edges(node* n, const edge_selector select, Dt_t* result);

Dt_t* select_incoming_edges(node* n, const edge_selector select, Dt_t* result);
/**
 * \brief Get the first node out of a CDT list.
 *
 * \param l A pointer to the list.
 * \return A pointer to the node container.
 */
NodeCtn* firstNode(Dt_t* l);

/**
 * \brief Get the next node out of a CDT list given a node.
 *
 * \param l A pointer to the list.
 * \param n A ponter to the node container.
 * \return A pointer to the next node container.
 */
NodeCtn* nextNode(Dt_t* l, NodeCtn* n);

/**
 * \brief Get the first edge out of a CDT list.
 *
 * \param l A pointer to the list.
 * \return A pointer to the edge container.
 */
EdgeCtn* firstEdge(Dt_t* l);

/**
 * \brief Get the next edge out of a CDT list given a edge.
 *
 * \param l A pointer to the list.
 * \param e A ponter to the edge container.
 * \return A pointer to the next edge container.
 */
EdgeCtn* nextEdge(Dt_t* l, EdgeCtn* e);


/**
 * Returns whether the given node has any outgoing edges.
 */
bool hasOutEdges(const node* n);

#endif
