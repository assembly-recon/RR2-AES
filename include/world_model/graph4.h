//
// Created by filip on 12.06.19.
//

// dict ptr key value pair dictionary to add properties dynamically
// string attributes for id, mid, mmid
// pload void *
// creation of new node only when semantics are modified, so payload with other
// structure is not a new node necessarily ownership of memory of payload
// belongs to application

#ifndef RT_GRAPHKUL_GRAPH3_H
#define RT_GRAPHKUL_GRAPH3_H

#include <cgraph.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef Agraph_t graph;
typedef Agnode_t node;
typedef Agedge_t edge;

typedef enum {
    PG_SID_FIELD_MAX_LENGTH = 32
} enum_PG_SID_FIELD_MAX_LENGTH;

typedef enum {
    PG_NODE_ID_MAX_LENGTH = (PG_SID_FIELD_MAX_LENGTH*3 + 10)
} enum_PG_NODE_ID_MAX_LENGTH;

/**
 * \struct sem_id_t
 * \brief A semantic ID.
 *
 * \var sem_id_t::id The unique ID of a node or edge.
 * \var sem_id_t::mid The model ID of a node or edge.
 * \var sem_id_t::mmid The meta-model ID of a node or edge.
 */
typedef struct {
  char id  [PG_SID_FIELD_MAX_LENGTH+1];
  char mid [PG_SID_FIELD_MAX_LENGTH+1];
  char mmid[PG_SID_FIELD_MAX_LENGTH+1];
} sem_id_t;

typedef struct {
    char id[PG_NODE_ID_MAX_LENGTH+1];
} node_id;

/* property handle (overhead) */
typedef enum {
  DEFINED = 0, /* default*/
  UNDEFINED = 1
} property_handle;

/**
 * \brief Create a property graph.
 * \return A pointer to the graph.
 *
 * Allocates memory to for a graph and returns a pointer to that memory.
 */
graph* create_property_graph();

/**
 * \brief Destroy a property graph.
 * \param g A pointer to the graph to be destroyed.
 * \return Integer status code.
 *
 * Destroys a previously allocated graph.
 */
int destroy_property_graph(graph* g);

// NODES
/**
 * \brief Create a node in the graph.
 * \param g A pointer to the graph.
 * \param sem_id_node A pointer to the semantic ID of the node.
 * \return A pointer to the created node.
 *
 * Given a semantic ID create a node in the graph.
 */
node* create_node(graph* g, sem_id_t* sem_id_node);

/**
 * \brief Destroy a node.
 * \param n A pointer to the node to be destroyed.
 * \return Integer status code.
 *
 * Destroys a previously allocated node.
 */
int destroy_node(graph* g, node* n);

/**
 * Convert a semantic ID to the unique ID used internally for the graph element.
 *
 *
 */
void sem_id_to_node_id(const sem_id_t* sid, node_id* dest);

/**
 * \brief Destroy a node by its' ID.
 * \param g A pointer to the graph.
 * \param id A pointer to the node's id.
 * \returns Integer status code.
 *
 * Destroys a previously allocated node by its' id.
 */
int destroy_node_by_id(graph* g, char* id);

/**
 * \brief Get a node by its' ID.
 * \param g A pointer to the graph.
 * \param id A pointer to the node's id.
 * \return A pointer to the node.
 *
 * Given a graph find a node by its' ID and return a pointer to it.
 */
node* get_node(graph* g, const char* id);

/**
 * Retrieve a node from its semantic ID
 */
node* get_node_by_sid(graph* g, const sem_id_t* sid);

/**
 * \brief Get the first node of the graph.
 * \param g A pointer to the graph.
 * \return A pointer to the node.
 *
 * Given a graph return the first defined node.
 */
node* first_graph_node(graph* g);

/**
 * \brief Get the next node on a graph given a node.
 * \param g A pointer to the graph.
 * \param n A pointer to a node.
 * \return A pointer to the next node.
 *
 * Given a graph and a node in the graph get the next node.
 */
node* next_graph_node(graph* g, node* n);

/**
 * \brief Given an object print its' semantic ID.
 *
 * \param n A pointer to the object.
 */
void print_sem_id(const void* obj);

/**
 * Get a copy of the semantic ID of a node/edge.
 * \param n A pointer to the node/edge.
 */
sem_id_t get_sem_id(const void* element);

/**
 * Get the `id` field of the semantic ID of the given element (node or edge)
 */
const char* get_sid_id  (const void* element);

/**
 * Get the `mid` field of the semantic ID of the given element (node or edge)
 */
const char* get_sid_mid (const void* element);

/**
 * Get the `mmid` field of the semantic ID of the given element (node or edge)
 */
const char* get_sid_mmid(const void* element);


/**
 * The unique string ID used internally in the graph for the given node.
 *
 * This is *not*, in general, the same as the `id` field of the semantic id
 * composite.
 */
const char* get_unique_id(const node*);

// EDGES
/**
 * \brief Create an edge between two nodes.
 * \param g A pointer to the graph.
 * \param sem_id_edge A pointer to the semantic ID of the edge.
 * \param n1 A pointer to the starting node of the edge.
 * \param n2 A pointer to the finishing node of the edge.
 * \return A pointer to the created edge.
 *
 * Creates an edge between two nodes.
 */
edge* create_edge(graph* g, sem_id_t* sem_id_edge, node* n1, node* n2);

/**
 * \brief Create an edge between two nodes given their ID.
 * \param g A pointer to the graph.
 * \param sem_id_edge A pointer to the semantic ID of the edge.
 * \param id_n1 A ponter to the ID of the starting node.
 * \parem id_n2 A pointer to the ID of the ending node.
 * \return A pointer to the created edge.
 *
 * Given the IDs of two nodes creates an edge between them.
 */
edge* create_edge_by_id(graph* g, sem_id_t* sem_id_edge, char* id_n1,
                        char* id_n2);

/**
 * \brief Destroy an edge.
 * \param e A pointer to the edge to be destroyed.
 * \return Integer status code.
 *
 * Destroys an edge between to nodes.
 */
int destroy_edge(graph* g, edge* e);

/**
 * \brief Find an edge given its' ID
 *
 * \param g A pointer to the graph.
 * \param n1 A pointer to the first node of the edge.
 * \param n2 A pointer to the second node of the edge.
 * \param id The ID of the edge.
 */
edge* find_edge_by_id(graph* g, node* n1, node* n2, char* id);

/**
 * \brief Get the semantic ID of an edge.
 * \param e A pointer to the edge.
 * \return The semantic ID of the edge.
 *
 * Given an edge get its' semantic ID.
 */
sem_id_t get_sem_id_edge(edge* e);

/**
 * Get the first property record of the given node.
 *
 * The returned pointer can be given to `property_name()` and `property_data()`
 * to get the name of the property and the pointer to the actual data object.
 *
 */
const void* get_first_property_record(const node* n);

/**
 * Get the property record that was added after the given one.
 *
 * Records are arranged in a circular fashion, so that iteration
 * eventually leads again to the first record. It is up to the
 * caller to possibly stop when this is happening.
 */
const void* get_next_property_record(const node* n, const void* current_prop);

/**
 * The name of the property stored in the given record
 */
const char* property_name(const void* property_record);

/**
 * The pointer to the data of the property stored in the given record.
 *
 * When the property name is known in advance, the function `get_node_property`
 * may be used instead of this function, to get the actual property data.
 */
const void* property_data(const void* property_record);

bool has_properties(const node*);

/**
 * \brief Create a property on an existing node.
 * \param n A pointer to the node.
 * \param name The name of the property, to be used later to reference the
 * property.
 * \param dim The size in bytes of the property data.
 * \return A pointer to the allocated property.
 *
 * Create a property on an existing node. It allocates the necessary memory and
 * returns a pointer to it.
 */
void* create_node_property(node* n, char* name, size_t dim);

/**
 * \brief Create an "undefined" property on an existing node.
 * \param n The pointer to the node.
 * \param property_name The name of the property.
 *
 * Creates an "undefined" property -- property exists but its type is unknown.
 *
 * TODO: @Niko do we need it???
 */
//void create_undef_property(node* n, char* name);

/**
 * \brief Check if a property of a node is undefined.
 * \param n The pointer to the node.
 * \param property_name The name of the property.
 * \return Integer status code.
 *
 * Checks if a property on a given node is undefined.
 *
 * TODO: @Niko Check in general the use consistency.
 */
//int is_undef_prop(node* n, char* name);

// TODO: @Niko we probably need to define an undefined property.
// /* remove an undefined property in place of a new one (ALLOC) */
// void*   replace_property(node* n, char* property_name, size_t dim);

/**
 * \brief Get a property from a node.
 * \param n A pointer to the node.
 * \param name The name of the property.
 * \return A pointer to the property.
 *
 * Given a node and a property's name get access to it.
 */
void* get_node_property(node* n, const char* name);

/**
 * \brief Get a constant pointer to a node's property.
 * \param n A pointer to the node.
 * \param name The name of the property.
 * \return A constant pointer to the property.
 *
 * Given a node and a property's name get constant access to it.
 */
const void* get_property_const(const node* n, const char* name);

/**
 * \brief Create a property on an existing edge.
 * \param n A pointer to the edge.
 * \param name The name of the property.
 * \param dim The size of the property.
 * \return A pointer to the created property.
 *
 * Given an edge add a property to it.
 */
void* create_edge_property(edge* e, char* name, size_t dim);

/**
 * \brief Get a property of an edge.
 * \param n A pointer to the edge.
 * \parem name The name of the property.
 * \return A pointer to the property.
 *
 * Given an edge and a property's name get a pointer to the property.
 */
void* get_edge_property(edge* n, char* name);

/**
 * \brief Get the ID of a node.
 * \param n A poiner to the node.
 * \return The node's ID.
 *
 * Given a node get its' ID.

 * DEPRECATED. Use `get_sid_id()`
 */
const char* get_node_id(const node* n);

/**
 * \brief Get the ID of an edge.
 * \param e A poiner to the edge.
 * \return The edge's ID.
 *
 * Given an edge get its' ID.
 *
 * DEPRECATED. Use `get_sid_id()`
 */
const char* get_edge_id(const edge* e);

/**
 * \brief Get the model ID of a node.
 * \param n A poiner to the node.
 * \return The node's model ID.
 *
 * Given a node get its' model ID.
 *
 * DEPRECATED. Use `get_sid_mid()`
 */
const char* get_node_mid(const node* n);

/**
 * \brief Get the model ID of an edge.
 * \param n A poiner to the edge.
 * \return The node's model ID.
 *
 * Given an edge get its' model ID.
 *
 * DEPRECATED. Use `get_sid_mid()`
 */
const char* get_edge_mid(const edge* e);

/**
 * \brief Get the meta-model ID of a node.
 * \param n A poiner to the node.
 * \return The node's meta-model ID.
 *
 * DEPRECATED. Use `get_sid_mmid()`
 */
const char* get_node_mmid(const node* n);

/**
 * \brief Get the meta-model ID of an edge.
 * \param n A poiner to the edge.
 * \return The node's meta-model ID.
 *
 * DEPRECATED. Use `get_sid_mmid()`
 */
const char* get_edge_mmid(const edge* e);

/**
 * \brief Get the node at the tail of an edge.
 *
 * \param e A pointer to the edge.
 * \return A pointer to the node.
 */
node* get_tail(edge* e);

/**
 * \brief Get the node at the head of an edge.
 *
 * \param e A pointer to the edge.
 * \return A pointer to the node.
 */
node* get_head(edge* e);

// VISUALIZATION
/**
 * \brief Save a graph in a file using DOT format.
 *
 * \param g A pointer to the graph.
 * \param filename The name of the file to save the graph.
 * \param verbose Toggle verbosity.
 */
void graph_to_dot(graph* g, char* filename, bool verbose);

/**
 * \brief Read a graph stored in a file using DOT format.
 * 
 * \param filename The name of the file containing the graph.
 * \return A pointer to the graph.
 */
graph* dot_to_graph(const char* filename);

#endif  // RT_GRAPHKUL_GRAPH3_H
