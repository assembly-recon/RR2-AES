# Assembly Execution System (AES) 
## Description
This project is version 1 of the AES.
Functionalities include:

* Composition and execution of Reconfigurable Petri Nets
* Communication with InfraFlex (ZeroMQ)
* Communication with ACR (CURL)
* World model based on CGraph

## Dependencies

* CGraph
    * Generic in-memory graph library: For DAG, Flow chart and petrinet construction and manipulation
    * Linux install ```$ sudo apt install libgraphviz-dev```
    
## Build

After installation of the dependencies, do
    
```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_INSTALL_PREFIX=</path/to/local_install_root> ..
$ make 
& make install
```


## Background

This version of the Assembly Execution System is a particular implementation of an execution system within the context of AssemblyRecon. As such, it is framed within the "cell area production" in the ISA Equipment Model, as shown in figure [below](diagrams/ISA95_equipment.png).

<img src="diagrams/ISA95_equipment.png" title="ISA95 Equipment model" width="50%" />

As a proof of concept, the AES is be able to:

* Execute orders with a worker pool pattern
	- Order comes in at any time
	- Initialized with a set of active resources assigned 
	- Petri net model initialized to progress track and coordinate involved active resources
	- Orders can be stopped and re-started at any stable point of the execution (according to priority of orders)
* Bidirectional communication with resources
	- Resources provides:
		* Declaration availability
		* Acceptance or rejection of orders according to current state of the resource
		* Report progress during execution
	- AES is in charge of:
		+ Dispatch orders to resources 
		+ Record progress of execution of orders from reported progress
		+ Record of state of the resources from reported progress on resource change tasks (e.g. toolchanging tasks)
* Track progress: tracks progress of executed order (batches) within the spawned petri nets per order.
* Reconfigure the execution online
	* Rush order case: in the case a rush order comes in, the AES manages the state of resources to stop in a stable state and preempt before taking the rush order. There are two modes, option 1) in which the ACR merges the rush order with the previously planned execution, or option 2) in which the rush order is taken as a disruption after which the AES does preemption task to go back to the previous execution.  
* Follow protocols for order execution
	- Preemption protocol: preempt resources when switching orders.
	- Stable states: do not interrupt the execution in unstable states.

## Functionalities 
### Coordination // Execution mediators

For discrete control of skill execution, Petri Nets allow asynchronous and concurrent execution of processes (“multi threading”); each of the latter can still use the more mainstream finite state machines for its own internal “single-threaded” coordination.

In this implementation,the places of Petri Nets represent the activities to be carried by the resources which perform a shared task, to perform the completion of an order. The places in the nets are connected to flag events for communicating with the resources which are coordinated (as the INFRAFlex setup with the communication introduced in the previous section). These events trigger discretely the execution of actions at the resource level. Moreover, these actions are represented in the Petri Net taking into account _stability_ concerns. This means that the construction of the Petri Net does not allow the interruption of the execution in _unstable_ states (eg. stopping the execution while an insertion is ongoing). 

The transitions of Petri Nets are meant to gather the conditions which trigger the activities represented in the net. This manner, the resources are only aware of the parts of the process in which they are required, not the entire execution process, allowing the synchronization via the Petri Net executor. An illustration of the composition of [Petri Nets](diagrams/petri_net_example.png)  can be seen below.

<img src="diagrams/petri_net_example.png" label="Petri Net example" width="45%"  />

The lightweight and modular composition of Petri Nets, allow the generation and merging of new Petri Nets while execution. 
The functionalities for the composition of these models are based on C code. The library also includes a generator of petri nets from the declaration of manufacturing orders as semantic graphs in jsonld. Examples of orders expressed in jsonld can be found in [ task specication](src/task_specification).

The functionalities for construction and use of Petri Nets can be found in [coordination](src/coordination):

* petrinet.c: functionalities to build ans use Petri Nets.
* petrinet_scheduler.c: functionalities and structures to synchronize with flag arrays.
* egraph-to-petri-net-state.cpp: functionalities to Reconfigurable Petri Nets with associated protocol for communication with resources.

#### Spawner

The AES is composed three types of threads: communication threads with the client (ACR) and the resources (InfraFlex), and order management threads called job executor orchestrators in the [figure](diagrams/aes_illustration.png) below. The communication threads are constructed as a generic interface with the different agents according to the protocols declared in the sections below. The executor threads, keep a series of Reconfigurable Petri Nets which executes in a sequential manner (though the execution sequence can change online according to reconfiguration protocols), which internally connect and synchronize the execution for the different resources. 

<img src="diagrams/aes_illustration.png" label="AES illustration" width="60%"  />

The main functionalities for the use of the AES can be found in [spawner](src/spawner):

* task_spawner.cpp: functionalities to activate and update Petri Nets in the AES.
* res_connection.cpp: generic thread for communicating with resources and pushing the information to the AES main threads.
* acr_connection.cpp: generic thread to receive orders from a client (usually ACR).
* AES_main.cpp: AES main code.
	- Starts the resource connection, client connection and job executor threads
	- Job executor threads spawn the Petri Nets (with executing agents and flag protocols) for order execution as batches/orders arrive from the client thread
	- Petri Nets are executed with "last in, first out" protocol 
	- Job executor sends the execution of the orders through the resource communication thread
	- Job executor tracks the progress of the orders and reports to the client
	- Job executor updates world model according to the reported information from the resource communication thread


### Communication 
#### Communication with the ACR

Communication between ACR-AES is stablished in 2 modes according to the protocol for handling rush orders. The protocol diverges from the nominal execution communication when the rush order is introduced.

The first mode (option 1) the rush order is managed by the ACR, handling the merge amid the current order and the rush order.
In this case, when the rush order is introduced, the ACR commands the pause of the resources, for making the merge of orders at the latest stable state of the system. Then the AES stops the system in a stable state and updates the ACR. When the ACR receives the update, it merges the execution left from the initial execution with the rush order and sends it to the AES, which executes the command on the resources. This is illustrated in the [figure](diagrams/ACR-AES-option1.jpg") below. 

<img src="diagrams/ACR-AES-option1.jpg" title="Communication ACR-AES: option 1 for rush order execution" />


The second mode (option 2) the rush order introduction is managed by the AES, by adding preemption actions. 
At communication with the ACR, the protocol is similar to the nominal protocol, with the introduction of the rush order while the execution of the other orders. This is illustrated in the [figure](diagrams/ACR-AES-option2.jpg) below.

![schema_2](diagrams/ACR-AES-option2.jpg "Communication ACR-AES: option 2 for rush order execution")

#### Communication with INFRAflex setup 

Communication between AES-InfraFlex setup is based on the [protocol](diagrams/AES_INFRA.svg) below.

<img src="diagrams/AES_INFRA.svg" label="Communication AES-INFRA" width="60%"  />

### World model

The world model is based on the CGraph library, which is one of the linux standard libraries. Functionalities to manage the graphs can be found in:

* graph4.c: wrapper over cgraph functionalities to add properies to the flat structure of the cgraph nodes.
* select.c: selector functionalities over cgraph.
* traverse.c: basic functionalities to show traversals in the graph.
* json-ld-parser-world-model.cpp: parser which constructs the initial world model from jsonld specification.

